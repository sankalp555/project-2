Feature: Business adds offer

  As an application owner
  I want businesses to add their offers to the page
  In order to offer buyers possibility for booking them

  Scenario: Business user chooses to add offer
    Given business user has accessed business page
    When user clicks 'Oglasi se'
    Then user can choose which kind of offer to add

  Scenario: Business user picks one offer to add
    Given business user has chosen to add offer
    When user chooses which offer to add
    And user clicks 'Potvrdi'
    Then a form is presented for user to enter relevant details

  Scenario: Business user adds an offer to application
    Given business user has picked which offer to add
    When user fills up the form
    And user clicks 'Potvrdi ponudu'
    Then an offer is saved to application and can be sold to users
