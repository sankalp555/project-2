Feature: User books vacation

  As an user,
  I want to be able to fill up the planner
  In order to book the vacation

  Scenario: User successfully books vacation
    Given the user has accessed application
    And user has provided payment details
    When user adds offer to planner
    And user clicks 'Book now'
    Then offer is booked
