Given(/^the user has accessed application$/) do
  page.driver.browser.basic_authorize('cfs', 'sfc')
  visit '/app'
end

Given(/^user has provided payment details$/) do
  @user = User.new(email: 'user@example.com', card_id: '12345678')
end

When(/^user adds offer to planner$/) do
  click_link 'Add to planner', match: :first
end

When(/^user clicks 'Book now'$/) do
  click_link 'Book now'
end

Then(/^offer is booked$/) do
  pending # check database for created ReservationPackage and Reservation
end
