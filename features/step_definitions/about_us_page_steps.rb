Given(/^that the user has accessed the homepage$/) do
  page.driver.browser.basic_authorize('cfs', 'sfc')
  visit '/marketing'
end

When(/^user clicks 'About us'$/) do
  click_link 'About us'
end

Then(/^user is redirected to About us page$/) do
  expect(current_path).to eq(about_path)
end
