Given(/^business user has accessed marketing page$/) do
  page.driver.browser.basic_authorize('cfs', 'sfc')
  visit '/marketing'
end

When(/^user clicks 'For business'$/) do
  click_link 'For business'
end

Then(/^user is redirected to business page$/) do
  expect(current_path).to eq(business_path)
end
