Given(/^the user has accessed marketing page$/) do
  basic = ActionController::HttpAuthentication::Basic
  credentials = basic.encode_credentials('cfs', 'sfc')
  page.driver.header('Authorization', credentials)

  visit '/marketing'
end

When(/^user clicks 'Sign up here'$/) do
  click_link 'Sign up here', match: :first
end

Then(/^user is presented with sign up form$/) do
  expect(page).to have_selector('div#signup-popup', visible: true)
end

Given(/^the user has accessed business page without having registered$/) do
  basic = ActionController::HttpAuthentication::Basic
  credentials = basic.encode_credentials('cfs', 'sfc')
  page.driver.header('Authorization', credentials)

  visit '/business'
end

Given(/^the user is presented with sign up form$/) do
  expect(page).to have_selector('div#signup-popup', visible: true)
end

Then(/^user has completed the sign up process$/) do
  expect(User.find_by(email: 'user@example.com').email).to
  eq('user@example.com')
end

When(/^user fills up the sign up form$/) do
  within 'div#signup-popup' do
    fill_in 'user_email', with: 'user@example.com'
  end
end

When(/^user clicks 'Sign up' in the sign up form$/) do
  within 'div#signup-popup' do
    click_button 'Sign up'
  end
end
