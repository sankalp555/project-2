Given(/^the user is located on the landing page \(homepage\)$/) do
  visit '/'
end

Given(/^the user has entered their e\-mail address in the form$/) do
  fill_in 'user_subscription_email', with: 'user@example.com'
end

When(/^user clicks 'Sign up'$/) do
  click_button 'Sign up'
end

Then(/^e\-mail address is saved to database$/) do
  @user = UserSubscription.find_by(email: 'user@example.com')
  expect(@user.email).to eq('user@example.com')
end

Then(/^page contains text 'Thank you for signing up'$/) do
  expect(page).to have_content(/Thank you for signing up/)
end
