Feature: Landing page subscription

  As an application owner,
  I want my users to leave me their e-mail,
  In order for me to contact them when application gets to beta stage

  Scenario: User (buyer) subscibes using form on landing page
    Given the user is located on the landing page (homepage)
    And the user has entered their e-mail address in the form
    When user clicks 'Sign up'
    Then e-mail address is saved to database
    And page contains text 'Thank you for signing up'