@javascript
Feature: Sign up

  As an application owner
  I want users to create an account for the application
  In order to be able to offer them full functionality of the application

  Scenario: User signs up through marketing page
    Given the user has accessed marketing page
    When user clicks 'Sign up here'
    Then user is presented with sign up form

  @ignore
  Scenario: User signs up through business page
    Given the user has accessed business page without having registered
    When user clicks 'Oglasi se'
    Then user is presented with sign up form

  Scenario: User completes the sign up process
    Given the user is presented with sign up form
    When user fills up the sign up form
    And user clicks 'Sign up' in the sign up form
    Then user has completed the sign up process