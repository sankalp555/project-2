Feature: About us page

  As an application owner,
  I want the website to have an About us page
  In order for visitors to find out more about our company

  Scenario: User visits About us page
    Given that the user has accessed the homepage
    When user clicks 'About us'
    Then user is redirected to About us page