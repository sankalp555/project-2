Feature: Business page

  As an application owner
  I want business users to have their own marketing page
  In order to present them features specific to business

  Scenario: Business user accesses business page
    Given business user has accessed marketing page
    When user clicks 'For business'
    Then user is redirected to business page