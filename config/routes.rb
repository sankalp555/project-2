Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  mount JasmineRails::Engine => '/specs' if defined?(JasmineRails)

  # 'static' pages
  root 'static#index'

  get 'business' => 'static#business'
  get 'about' => 'static#about'
  get 'signed_up' => 'static#signed_up'

  post 'user_subscriptions' => 'user_subscriptions#create'

  # application-related routes
  devise_for :user, controllers: { sessions: 'users/sessions',
                                   registrations: 'users/registrations',
                                   passwords: 'users/passwords',
                                   confirmations: 'users/confirmations' }

  get 'app/accommodation_type' => 'planner#accommodation_type', as: 'planner_accommodation_type'
  get 'app/activity_type' => 'planner#activity_type', as: 'planner_activity_type'
  get 'app/hotels' => 'planner#hotels', as: 'planner_hotels'
  get 'app/hostels' => 'planner#hostels', as: 'planner_hostels'
  get 'app/apartments' => 'planner#apartments', as: 'planner_apartments'
  get 'app/activities' => 'planner#activities', as: 'planner_activities'
  get 'app/events' => 'planner#events', as: 'planner_events'
  get 'app/vehicles' => 'planner#vehicles', as: 'planner_vehicles'
  get 'app/planner_add_activity/:type/:id' => 'planner#add_activity', as: 'planner_add_activity'
  get 'app/planner_remove_activity/:type/:id' => 'planner#remove_activity', as: 'planner_remove_activity'
  get 'app/planner_add_accommodation/:id' => 'planner#add_accommodation', as: 'planner_add_accommodation'
  get 'app/planner_remove_accommodation' => 'planner#remove_accommodation', as: 'planner_remove_accommodation'
  get 'app/planner_new_session' => 'planner#new_session', as: 'planner_new_session' 
  get 'app/book' => 'planner#book', as: 'planner_book'
  get 'app/init' => 'planner#init', as: 'planner_init'
  get 'app(/:date/:destination)' => 'planner#list_chosen', as: 'planner_list_chosen'
  get 'app/get_service_details' => 'planner#get_service_details'
  get 'app/planner_edit_session' => 'planner#edit_session', as: 'planner_edit_session'
  post 'app/planner_update_session' => 'planner#update_session', as: 'planner_update_session'

  resource :user_profile

  # listings resources are used for browsing resources
  # does not support modifying or deleting
  resources :listings, only: [ :index, :show ]

  # user_listings resources are used for managing single users' listings:
  # creation, editing, listing, deleting, ...
  resources :user_listings, only: [ :index, :new, :show, :destroy, :edit ]

  resources  :message_threads, only: [ :index, :new, :create ] do
    resources :messages, only: [ :index, :show, :create ]
  end
  
  resources :reservations, only: [ :index, :show]
  get 'reservations/:id/accept' => 'reservations#accept',
      as: 'reservation_accept'
  get 'reservations/:id/reject' => 'reservations#reject',
      as: 'reservation_reject'
  get 'reservations/:id/cancel' => 'reservations#cancel',
      as: 'reservation_cancel'
  resources :reservation_packages, only: [ :show, :index ] do
    resources :reservations, only: [ :index, :show ]
    get 'reservation_packages/download' => 'reservation_packages#download', as: 'download'
  end

  resources :photographs, only: [ :update, :destroy ]

  # lodging providers and associated resources
  resources :hotels do
    shallow do
      resources :photographs, only: [ :create ]

      resources :rooms do
        resource :pricing_scheme, controller: 'room_pricing_schemes'
        resource :calendar, only: [ :show ]
        resources :calendar_intervals
        resource :photographs, only: [ :create, :update ]
      end
    end
  end

  resources :hostels do
    shallow do
      resource :photographs, only: [ :create ]

      resources :rooms do
        resource :pricing_scheme, controller: 'room_pricing_schemes'
        resource :calendar, only: [ :show ]
        resources :calendar_intervals
        resource :photographs, only: [ :create ]
      end
    end
  end

  resources :private_accommodations do
    shallow do
      resource :photographs, only: [ :create ],
               controller: 'private_accommodations', action: 'create_photograph'

      resources :rooms do
        resource :pricing_scheme, controller: 'room_pricing_schemes'
        resource :calendar, only: [ :show ]
        resources :calendar_intervals
        resource :photographs, only: [ :create, :update]
      end
    end
  end

  resources :lighthouses do
    shallow do
      resource :photographs, only: [ :create ]
      resource :calendar, only: [ :show ]
      resources :calendar_intervals
      resource :pricing_scheme, controller: 'lighthouse_pricing_schemes'
    end
  end

  resources :islands do
    shallow do
      resource :photographs, only: [ :create ]
      resource :calendar, only: [ :show ]
      resources :calendar_intervals
      resource :pricing_scheme, controller: 'island_pricing_schemes'
    end
  end

  resources :vehicles do
    shallow do
      resources :photographs, only: [ :create ]
    end
  end

  resources :rents do
    shallow do
      resource :photographs, only: [ :create ]
      resources :cars do
        resource :calendar, only: [ :show ]
        resource :pricing_scheme, controller: 'rent_pricing_schemes'
        resources :calendar_intervals
        resource :photographs, only: [ :create ]
      end
      resources :boats do
        resource :calendar, only: [ :show ]
        resource :pricing_scheme, controller: 'rent_pricing_schemes'
        resources :calendar_intervals
        resource :photographs, only: [ :create ]
      end
      resources :bikes do
        resource :calendar, only: [ :show ]
        resource :pricing_scheme, controller: 'rent_pricing_schemes'
        resources :calendar_intervals
        resource :photographs, only: [ :create ]
      end
      resources :motorbikes do
        resource :calendar, only: [ :show ]
        resource :pricing_scheme, controller: 'rent_pricing_schemes'
        resources :calendar_intervals
        resource :photographs, only: [ :create ]
      end
    end
  end

  # events
  resources :events do
    shallow do
      resource :photographs, only: [ :create ]
    end
  end

  # activities
  resources :activities do
    shallow do
      resource :photographs, only: [ :create ]
      resources :calendar_intervals
      resource :calendar, only: [ :show ]
      resource :pricing_scheme, controller: 'activity_pricing_schemes'
    end
  end

  # Charterers
  resources :charters do
    shallow do
      resource :photographs, only: [ :create ]
      resources :calendar_intervals
      resource :calendar, only: [ :show ]
      resource :pricing_scheme, controller: 'charter_pricing_schemes'
    end
  end

  get '/mangopay_webhook', :to => 'trips#mangopay_webhook'

  resources :payments

  # Trips
  resources :trips do
    member do
      get :list_chosen
      get :book
      get :mangopay_webhook
      get :reserve_payment
      post :register_card
    end
    resources :trip_destinations
  end

  resources :trip_destinations do
    member do
      get :accommodation_type
      get :activity_type
    end
    resources :trip_items, :only => [:index] do
      collection do
        get :add_accommodation
        get :add_activity
      end
    end
  end
  
  resources :trip_items, :only => [:show, :destroy]
  
  resources :cities, :only => [:index] do
    get :autocomplete_city_name, :on => :collection
  end

end
