root = "/home/deployer/croffers/current"
working_directory root
pid "/var/run/unicorn/unicorn.pid"
stderr_path "/var/log/unicorn-error.log"
stdout_path "/var/log/unicorn-access.log"

listen "/var/run/unicorn/croffers.sock"
worker_processes 2
timeout 30
