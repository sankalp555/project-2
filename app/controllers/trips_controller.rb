class TripsController < ApplicationController
  layout :set_layout
  #before_action :authenticate #, :authenticate_user!
  before_action :authenticate_user!, :only => [:book, :reserve_payment]

  def index
    @trips = current_user.trips.draft.order("id desc")
  end

  def new
    if current_user.present?
      trips = current_user.trips.draft
      if trips.count >= 4
        flash[:notice] = "Možete stvoriti samo četiri plana odjednom. Odaberite bilo kojeg da biste nastavili ili izbrisali planove na čekanju!"
        redirect_to trips_path
      else
        @trip = Trip.new
        @trip_destination = @trip.trip_destinations.build
      end
    else
      if !cookies[:trip].nil?
        cookies_trip = JSON.parse(cookies[:trip])
        if Trip.exists?(cookies_trip[0]["trip_id"]) 
          redirect_to edit_trip_path(cookies_trip[0]["trip_id"])
        end
      end
      @trip = Trip.new
      @trip_destination = @trip.trip_destinations.build
    end
  end

  def edit
    @trip = Trip.find(params[:id])
  end

  def create
    @trip = Trip.new(trip_params)
    if @trip.save
      if !current_user.present?
        @trip.update(expires: 1.week.from_now.to_datetime)
        cookies[:trip] = {:value => JSON.generate([trip_id: @trip.id]), :expires => 1.week.from_now }
      end
      redirect_to list_chosen_trip_path(@trip)
    else
      render 'new'
    end
  end

  def update
    @trip = Trip.find(params[:id])
    if @trip.update_attributes(trip_params)
      redirect_to list_chosen_trip_path(@trip)
    else
      render 'edit'
    end
  end
  
  def list_chosen
    @trip = Trip.includes(:trip_destinations).find(params[:id])
    @trip_destinations = @trip.trip_destinations
    @all_dates_destinations = @trip_destinations.collect_concat do |d|
      ts_range = Date.parse(d.start_date.strftime('%Y-%m-%d'))..Date.parse(d.end_date.strftime('%Y-%m-%d'))
      dates = ts_range.to_a
      dates.each.collect_concat do |date|
        { date: date, city_id: d.city_id, id: d[:id] }
      end
    end.sort do |a, b|
      a[:date] <=> b[:date]
    end
  end
  
  def reserve_payment
    @trip = Trip.find(params[:id])
  end
  
  def register_card
    # @trip = Trip.find(params[:id])
    @card_registration = CardRegistrationService.new(params[:id],current_user,root_url)
    pre_auth_payment_response = @card_registration.initiate_card_registration(register_card_params)
    if !pre_auth_payment_response["SecureModeRedirectURL"].nil?
      redirect_to pre_auth_payment_response["SecureModeRedirectURL"]
    else
      flash[:payment_error] = pre_auth_payment_response["ResultMessage"]
      redirect_to :back
    end    
  end
  
  # def book
  #   @trip = Trip.find(params[:id])
  #   response = @trip.create_reservation_package(current_user, root_url)
  #   if !response["SecureModeRedirectURL"].nil?
  #     redirect_to response["SecureModeRedirectURL"]
  #   else
  #     flash[:payment_error] = response["ResultMessage"]
  #     redirect_to :back
  #   end
  # end

  def mangopay_webhook
    if !params.empty?
      # payment_response = MangoPay::PreAuthorization.fetch(params["preAuthorizationId"])
      render text: "Success"
    else
      render text: "Transaction Failed!"
    end
    # if !params.empty?
    #   payment_response = MangoPay::PayIn.fetch(params["RessourceId"])
    #   payment = Payment.find_by(transaction_id: params["RessourceId"])
    #   payment.update(event_type: params["EventType"], payment_status: payment_response["Status"])
      
    #   if payment.payment_status == "SUCCEEDED"
    #     generate_pdf(payment.invoice)
    #   end
      
    #   render text: "Success"
    # else
    #   render text: "Transaction Failed!"
    # end
  end

  def generate_pdf(invoice)
    pdf_kit = PDFKit.new(as_html(invoice.id), page_size: 'A4')
    pdf_kit.to_file("#{Rails.root}/public/invoice-#{invoice.payment.transaction_id}.pdf")
    file = File.open("#{Rails.root}/public/invoice-#{invoice.payment.transaction_id}.pdf")
    invoice_attachment = invoice.photographs.new(photograph_file_name: "#{invoice.payment.transaction_id}.pdf")
    invoice_attachment.photograph = file
    invoice_attachment.save
    if invoice_attachment.save
      UserMailer.send_invoice(invoice.payment.user_id, invoice.id).deliver_now
      File.delete(file) if File.exist?(file)
    end
  end

  def as_html(id)
    @invoice = Invoice.find(id)
    render_to_string template: "payments/invoice_template.html.erb", layout: "invoice"
  end
  
  def destroy
    @trip = Trip.find(params[:id])
    @trip.destroy
    redirect_to trips_path  
  end

  protected
  
  def trip_params
    params.require(:trip).permit(:no_of_guest, :start_date, :end_date, trip_destinations_attributes: [:id, :start_date, :end_date, :city_id, :no_of_guest, :_destroy]).merge(user_id: current_user.present? ? current_user.id : nil )
  end

  def register_card_params
    params.permit(:username, :card_number, :month, :year, :cvv)
  end
  # def authenticate
  #   authenticate_or_request_with_http_basic do |id, password| 
  #      id == USER_ID && password == PASSWORD
  #   end
  # end

  def set_layout
    (params[:action] == "index") ? "user_profile" : "new_trip"
  end
  
end
