class Users::SessionsController < Devise::SessionsController
# before_filter :configure_sign_in_params, only: [:create]
  layout 'show'

  # GET /resource/sign_in
  # def new
  #   super
  # end

  # POST /resource/sign_in
  # def create
  #   super
  # end

  # DELETE /resource/sign_out
  # def destroy
  #   super
  # end

  # protected

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_sign_in_params
  #   devise_parameter_sanitizer.for(:sign_in) << :attribute
  # end

  def after_sign_in_path_for(resource)
    if !cookies[:trip].nil?
      cookies_trip = JSON.parse(cookies[:trip])
      @trip = Trip.find cookies_trip[0]["trip_id"]
      @trip.update(user_id: resource.id)
      if @trip.total_amount > 0
        trip_to = reserve_payment_trip_path(@trip)
      else
        trip_to = list_chosen_trip_path(@trip)
      end
      cookies.delete(:trip)
    end
    trips = current_user.trips.draft
    if trips.count > 0
      flash[:draft_msg] = %Q[Otkrili smo da na vašem računu imate nekoliko nacrta putovanja koje možete pregledati i nastaviti. <a href="/trips">Kliknite ovdje </a> da biste vidjeli popis vaših planova putovanja.]
    end
    @trip.present? ? trip_to : edit_user_profile_path
  end

  def after_sign_out_path_for(resource)
    root_path
  end
end
