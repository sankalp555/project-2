class StaticController < ApplicationController
  layout 'static'

  def index
    render 'static/index'
  end

  def about
    @user = UserSubscription.new

    render 'static/about'
  end

  def business
    render 'static/business'
  end

  def signed_up
    render 'static/signed_up'
  end
end
