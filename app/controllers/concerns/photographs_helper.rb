module PhotographsHelper
  def getPhotoUrls (asset)
    urls = []

    asset.photographs.each do |p|
      urls << {
        id: p.id,
        url: p.photograph.url(:thumb),
        post_url: photograph_path(p),
        seq_no: p.sequence_number
      }
    end

    urls.sort_by! { |p| p[:seq_no] }

    return urls.to_json
  end
end
