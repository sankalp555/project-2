class TripItemsController < ApplicationController
  layout "new_trip"

  def index
  end

  def show
    @trip_destination = TripDestination.find(params[:trip_destination_id])
    @destination = params[:destination]
    @ts_range = params[:ts_range]
    date_range = @ts_range.split("..")
    if params[:item_type] == "Room" || params[:item_type] == "Island" || params[:item_type] == "Lighthouse" || params[:item_type] == "Charter"
      @accommodation = @trip_destination.trip_items.find(params[:id])
      @accommodation_details = @accommodation.item_type.constantize.find(@accommodation.item_id)
      render :partial => 'accommodation'
    else
      @activity = @trip_destination.trip_items.find(params[:id])
      @activity_details = @activity.item_type.constantize.find(@activity.item_id)
      render :partial => 'activity_details'
    end
  end
  
  def destroy
    trip_item = TripItem.find(params[:id])
    trip_item.destroy
    redirect_to list_chosen_trip_path(trip_item.trip_destination.trip)
  end

  def add_accommodation
    @trip_destination = TripDestination.find(params[:trip_destination_id])
    add_accommodation_item params[:listing_type], params[:listing_id], params[:trip_destination_id], params[:ts_range]
    redirect_to list_chosen_trip_path(@trip_destination.trip)
  end

  def add_activity
    @trip_destination = TripDestination.find(params[:trip_destination_id])
    add_activity_item params[:listing_type], params[:listing_id], params[:trip_destination_id], params[:date]
    redirect_to list_chosen_trip_path(@trip_destination.trip)
  end

  def add_accommodation_item(type, id, trip_destination_id, ts_range)
    dates = ts_range.split("..")
    obj = type.constantize.find(id)
    price = ReservablePriceQuery.new(obj).get_price(dates[0].to_date..dates[1].to_date)
    @trip_item = TripItem.create(item_type: type, item_id: id, price: price, trip_destination_id: trip_destination_id, start_date:dates[0].to_date, end_date:dates[1].to_date)
  end

  def add_activity_item(type, id, trip_destination_id, date)
    if type == 'Event'
      ci = Event.find(id)
      price = ci.base_rate
    else
      ci = type.constantize.find(id).calendar_intervals.includes(:schemable).containing_date(date.to_date)
      price = ci.last.schemable.base_rate
    end
    @trip_item = TripItem.create(item_type: type, item_id: id, price: price, trip_destination_id: trip_destination_id, start_date: date.to_date, end_date: date.to_date)
  end

  protected

  def trip_params
    params.require(:trip_destination).permit(:start_date, :end_date, :destination, :no_of_guest)
  end

end
