class TripDestinationsController < ApplicationController
  layout :apply_layout

  def index
    @trip = Trip.find(params[:trip_id])
    @trip_destinations = @trip.trip_destinations
    @trip_destination = TripDestination.new
    @last_destination = @trip_destinations.last
  end

  def create
    @trip = Trip.find(params[:trip_id])
    @trip_destination = @trip.trip_destinations.create(trip_params)
    if @trip_destination.save
      if params[:commit] == "Find"
        redirect_to list_chosen_trip_path(@trip)
      else
        redirect_to trip_trip_destinations_path(@trip)
      end
    else
      if @trip.trip_destinations.reload.present?
        redirect_to list_chosen_trip_path(@trip)
      else
        flash[:errors] = @trip_destination.errors.full_messages
        redirect_to :back
      end
    end
  end

  def edit
    @trip = Trip.find(params[:trip_id])
    @trip_destination = TripDestination.find(params[:id])
    @trip_destinations = @trip.trip_destinations
  end
  
  def update
    @trip_destination = TripDestination.find(params[:id])
    @trip_destination.update(trip_params)
    if params[:commit] == "Find"
      redirect_to list_chosen_trip_path(@trip_destination.trip)
    else
      redirect_to trip_trip_destinations_path(@trip_destination.trip)
    end
  end

  def destroy
    @trip_destination = TripDestination.find(params[:id])
    trip_id = @trip_destination.trip_id
    @trip_destination.destroy
    redirect_to trip_trip_destinations_path(trip_id)
  end

  def accommodation_type
    @trip = Trip.find(params[:trip_id])
    @ts_range  = params[:ts_range]
    range = @ts_range.split("..")
    @city_id = params[:city_id]
    date_from = range[0].to_date
    date_to   = range[1].to_date
    hotels = hostels = private_accommodations = islands = lighthouses = charters = []
    hotels = Room.includes(:photographs).hotel.available(date_from, date_to, @city_id) if ['', 'all', 'hotels'].include?(params[:type].to_s)
    hostels = Room.includes(:photographs).hostel.available(date_from, date_to, @city_id) if ['', 'all', 'hostels'].include?(params[:type].to_s)
    private_accommodations = Room.includes(:photographs).private_accommodation.available(date_from, date_to, @city_id) if ['','all','private_accommodations'].include?(params[:type].to_s)
    islands = Island.includes(:photographs).available(date_from, date_to, @city_id) if ['','all','islands'].include?(params[:type].to_s)
    lighthouses = Lighthouse.includes(:photographs).available(date_from, date_to, @city_id) if ['','all','lighthouses'].include?(params[:type].to_s)
    charters = Charter.includes(:photographs).available(date_from, date_to, @city_id) if ['','all','charters'].include?(params[:type].to_s)
    @listings = hotels + hostels + private_accommodations + islands + lighthouses + charters
    render 'listings'
  end

  def activity_type
    @trip = Trip.find(params[:trip_id])
    @date = params[:date]
    @city_id = params[:city_id]
    activities = events = vehicle = []
    activities = Activity.includes(:photographs).available(@date, @city_id) if ['', 'all', 'activities'].include?(params[:type].to_s)
    events = Event.includes(:photographs).available(@city_id) if ['', 'all', 'events'].include?(params[:type].to_s)
    vehicle = Vehicle.includes(:photographs).available(@date, @city_id) if ['', 'all', 'vehicle'].include?(params[:type].to_s)
    @activities = activities + events + vehicle
    render 'activities'
  end

  protected

  def trip_params
    params.require(:trip_destination).permit(:start_date, :end_date, :destination, :no_of_guest)
  end

  def apply_layout
    if ["new","edit","index"].include?(action_name)
      "new_trip"
    else
      "application"
    end
  end
end
