ActiveAdmin.register Hostel do
  config.clear_action_items!
  menu parent: "Businesses", label: "Hostels"
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end

  index do
    selectable_column
    id_column
    column :user, -> (activity) {activity.owner}
  column :name
    column :description
    column :check_in
    column :check_out
    column :non_stop_reception
    column :non_stop_security
    column :adoptors  
    column :air_conditioning 
    column :airport_transfers
    column :atm
    column :bar
    column :bbq
    column :bicycle_hire
    column :bicycle_parking
    column :board_games
    column :book_exchange
    column :breakfast
    column :cable           
    column :cafe
    column :ceiling_fan
    column :childrens_play
    column :common_room
    column :cooker
    column :cots
    column :exchange
    column :direct_dial_tel
    column :dishwasher
    column :dryer
    column :dvds
    column :elevator
    column :express
    column :fax
    column :city_maps
    column :city_tour
    column :internet
    column :parking
    column :fridge
    column :fusball
    column :games
    column :gym
    column :hairdryers
    column :hairdryers_hire
    column :hot_showers
    column :hot_tub
    column :housekeeping
    column :pool_indoor
    column :internet_access
    column :internet_cafe
    column :iron_board
    column :jobs_board
    column :key_card
    column :kitchen
    column :late_checkout
    column :laundry_facilities
    column :linen_included
    column :linen_not_included
    column :lockers
    column :luggage
    column :meals
    column :meeting
    column :microwave
    column :supermarket
    column :nightclub
    column :pool_outdoor
    column :terrace
    column :playstation
    column :pool
    column :postal
    column :reading_light
    column :reception
    column :restaurant
    column :safe
    column :sauna
    column :shuttle
    column :steam_room
    column :swimming_pool
    column :tea_coffee
    column :travel_desk
    column :towels_hire
    column :towels_included
    column :towels_not_included
    column :utensils
    column :vending_machines
    column :wake_up_calls
    column :washing_machine
    column :wheelchair
    column :wifi
    column :wii
    column :created_at
    column :updated_at
    actions
  end
end
