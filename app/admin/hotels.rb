ActiveAdmin.register Hotel do
  config.clear_action_items!
  menu parent: "Businesses", label: "Hotels"
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end
  index do
    selectable_column
    id_column
    column :user, -> (activity) {activity.owner}
    column :name
    column :description
    column :check_in
    column :check_out
    column :front_desk
    column :adult_only
    column :air_conditioning
    column :allergy_free
    column :non_smoking_all
    column :bar
    column :breakfast_buffet
    column :chapel
    column :smoking_area
    column :design_hotel
    column :elevator
    column :express_check
    column :family_rooms
    column :garden
    column :gay_friendly
    column :heating
    column :luggage_storage
    column :mini_market
    column :newspapers
    column :non_smoking_rooms
    column :private_beach
    column :restaurant
    column :restaurant_carte
    column :restaurant_buffet
    column :disabled
    column :safe
    column :kitchen
    column :shops
    column :ski_storage
    column :snack_bar
    column :soundproof
    column :sun_terrace
    column :terrace
    column :valet_parking
    column :aqua
    column :bbq
    column :beachfront
    column :billiards
    column :bowling
    column :canoeing
    column :casino
    column :playground
    column :cycling
    column :darts
    column :diving
    column :evening_entertainment
    column :fishing
    column :fitness
    column :games
    column :golf
    column :hiking
    column :riding
    column :bath
    column :tub
    column :pool_all
    column :pool_season
    column :pool_swimming
    column :karaoke
    column :kids_club
    column :library
    column :massage
    column :minigolf
    column :out_pool_all
    column :out_pool_season
    column :sauna
    column :skiing
    column :ski_school
    column :snorkeling
    column :solarium
    column :spa
    column :squash
    column :table_tennis
    column :tennis
    column :steam
    column :windsurfing
    column :airport_shuttle
    column :atm
    column :babysitting
    column :barber
    column :bicycle
    column :room_breakfast
    column :bridal_suite
    column :business_centre
    column :car_rental
    column :concierge
    column :exchange
    column :maid
    column :dry_cleaning
    column :entertainment_staff
    column :fax
    column :groceries
    column :ironing
    column :laundry
    column :lockers
    column :meeting
    column :nightclub
    column :lunches
    column :private_check
    column :room_service
    column :lounge
    column :shoe_shine
    column :shuttle
    column :ski_hire
    column :ski_pass
    column :ski_to_door
    column :souvenirs
    column :special_diet
    column :tickets
    column :tour
    column :trouser_press
    column :vending_drinks
    column :vending_snacks
    column :vip_room
    column :water_sports
    column :created_at
    column :updated_at
    actions
  end
end
