ActiveAdmin.register Reservation do
  config.clear_action_items!

  index do
    selectable_column
    id_column
	column "Reservable" do |r|
	  link_to r.reservable.name, polymorphic_url([:admin, r.reservable])
	end
	column :reservable_type
	column :reservation_package
	column :date_from
	column :date_to
	column :number_of_people
	column :price do |r|
		"€#{r.price}"
	end
	column :status
    
    actions
  end

end
