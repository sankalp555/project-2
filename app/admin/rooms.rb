ActiveAdmin.register Room do
  config.clear_action_items!
  menu parent: "Businesses", label: "Rooms"
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end
  index do
    selectable_column
    id_column
    column :user, -> (activity) {activity.owner}
    column :lodging_provider
    column :lodging_provider_type
    column :name
    column :description
    column :air_conditioning
    column :cleaning_products
    column :walk_in_closet
    column :fan
    column :hot_tub
    column :ironing
    column :priv_ent
    column :sitting
    column :tile_floor
    column :parquet_floor
    column :hypoallergenic
    column :dryer
    column :el_blankets
    column :fireplace
    column :interconnecting
    column :mosquito
    column :pool
    column :sofa
    column :wardrobe
    column :carpeted
    column :desk
    column :long_beds
    column :heating
    column :iron
    column :suit
    column :safe
    column :soundproof
    column :wash
    column :additional
    column :bathtub_shower
    column :toiletries
    column :shared_bathroom
    column :slippers
    column :bathroom
    column :guest_bathroom
    column :bathrobe
    column :hairdryer
    column :shared_toilet
    column :spa_tub
    column :bathtub
    column :bidet
    column :sauna
    column :shower
    column :toilet
    column :blu_ray
    column :computer
    column :flat_tv
    column :console
    column :console_ps2
    column :console_ps3
    column :console_ps4
    column :console_wii
    column :console_xbox
    column :laptop
    column :radio
    column :telephone
    column :ipad
    column :cd_play
    column :dvd_play
    column :laptop_safe
    column :satellite
    column :video
    column :ipod_dock
    column :cable
    column :fax
    column :pay_channels
    column :tv
    column :video_games
    column :max_adults
    column :max_children
    column :size
    column :single_beds
    column :double_beds
    column :queen_beds
    column :king_beds
    column :bunk_beds
    column :sofa_beds
    column :futons
    column :barbecue
    column :dining_table
    column :high_chair
    column :kitchenware
    column :outdoor_dining
    column :refrigerator
    column :toaster
    column :coffee
    column :dishwasher
    column :kitchen
    column :microwave
    column :outdoor_furniture
    column :stovetop
    column :dining
    column :kettle
    column :kitchenette
    column :minibar
    column :oven
    column :tea_coffee
    column :alarm_clock
    column :towels
    column :executive
    column :towels_extra
    column :linens
    column :wake_up
    column :city_view
    column :landmark_view
    column :pool_view
    column :terrace
    column :garden_view
    column :mountain_view
    column :river_view
    column :view
    column :lake_view
    column :patio
    column :sea_view
    column :balcony
    column :smoking_allowed
    column :created_at
    column :updated_at
    actions
  end
end
