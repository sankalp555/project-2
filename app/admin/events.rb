ActiveAdmin.register Event do
  config.clear_action_items!  
  menu parent: "Businesses", label: "Events"
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end
  index do
    selectable_column
    id_column
    column :user, -> (activity) {activity.owner}
    column :name
    column :description
    column :capacity
    column :base_rate
    column :ts_range
    actions
  end
end
