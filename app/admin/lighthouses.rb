ActiveAdmin.register Lighthouse do
  config.clear_action_items!
  menu parent: "Businesses", label: "Light Houses"
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end
  index do
    selectable_column
    id_column
    column :user, -> (activity) {activity.owner}
    column :name
    column :description
    column :check_in
    column :check_out
    column :max_adults
    column :max_children
    column :single_beds
    column :double_beds
    column :created_at
    column :updated_at
    actions
  end
end
