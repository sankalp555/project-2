ActiveAdmin.register Payment do
  config.clear_action_items!

  index do
    selectable_column
    id_column
    column :transaction_id
    column :amount do |p|
      "€#{p.amount}"  
    end
    column :fee do |p|
      "€#{p.fee}"
    end
    column :payment_type
    column :credited_wallet_id
    column :credited_user_id
    column :payment_status
    column :reservation_package
    column :user
    column :created_at
    column :updated_at
    actions
  end

end
