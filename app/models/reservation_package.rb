# coding: utf-8
class ReservationPackage < ActiveRecord::Base
  belongs_to :user
  has_one :payment
  has_many :reservations, dependent: :destroy

  validates :date_from, presence: true
  validates :date_to, presence: true

  def get_daterange
    if self.date_from == self.date_to
      return self.date_from.strftime('%d. %-m.')
    else
      return "#{self.date_from.strftime('%d. %-m.')} - #{self.date_to.strftime('%d. %-m.')}"
    end
  end

  def get_price
    self.reservations.confirmed.sum(:price)
  end                              

  def get_destination
    room_reservations = self.reservations.accommodation
    
    if room_reservations.first
      room_reservations.first.reservable.lodging_provider
        .address.city_id
    else
      self.reservations.first.reservable.address.city_id
    end
  end

  def create_payment(user, payment)
    payment_params = {
      transaction_id: payment["Id"],
      payment_status: payment["Status"],
      amount: payment["DebitedFunds"]["Amount"],
      currency: payment["DebitedFunds"]["Currency"],
      fee: 0,
      payment_type: payment["PaymentType"],
      payment_tag: payment["Tag"],
      user_id: user.id,
      reservation_package_id: id
    }

    payment = Payment.new(payment_params)
    if payment.save
      invoice = Invoice.new(user_id: user.id, payment_id: payment.id, amount: payment.amount, fee: payment.fee)
      if invoice.save
        reservations.each do |r|
          invoice.invoice_lines.create(reservation_id: r.id, name: "#{r.reservable_type} - #{r.reservable.name}", amount: r.price)
        end
      end
    end
  end
end
