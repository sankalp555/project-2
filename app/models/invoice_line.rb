class InvoiceLine < ActiveRecord::Base
  belongs_to :invoice
  belongs_to :reservation

  after_save :update_invoice_amount

  def update_invoice_amount
  	total = invoice.invoice_lines.sum(:amount)
  	invoice.update_attributes(amount: total)
  end
end
