class TripDestination < ActiveRecord::Base
  belongs_to :trip
  belongs_to :city
  has_many :trip_items, dependent: :destroy

  validates :start_date, presence: :true
  validates :end_date, presence: :true
  validates :city_id, presence: :true
end
