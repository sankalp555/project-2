class Trip < ActiveRecord::Base
  belongs_to :user
  has_many :trip_destinations, dependent: :destroy
  accepts_nested_attributes_for :trip_destinations, reject_if: :all_blank, allow_destroy: true
  
  validates :no_of_guest, presence: :true
  
  scope :draft, -> { where(:active => false) }
  
  def create_reservation_package(user, url, card_id)
    payment = {}
    start_date = trip_destinations.pluck(:start_date).first
    last_date = trip_destinations.pluck(:end_date).last
    rp = ReservationPackage.new(user_id: user.id, date_from: start_date, date_to: last_date)
    if rp.save
      self.update_attributes(active: true)
      trip_items = TripItem.where("trip_destination_id IN (?)", trip_destinations.pluck(:id))
      trip_items.each do |trip_item|
        rp.reservations.create(
          reservable_id: trip_item.item_id,
          reservable_type: trip_item.item_type,
          date_from: trip_item.start_date,
          date_to: trip_item.end_date,
          number_of_people: trip_item.trip_destination.no_of_guest,
          price: trip_item.price
        )
      end
      payment = MangoPay::PreAuthorization.create({
        AuthorId: ENV['AUTHOR_ID'],
        DebitedFunds: { Currency: ENV['CURRENCY'], Amount: trip_items.sum(:price).to_i*100 },
        Fees: { Currency: ENV['CURRENCY'], Amount: 0 },
        CardId: card_id,
        SecureMode: 'DEFAULT',
        SecureModeReturnURL: "#{url}/reservation_packages/#{rp.id}/reservations",
        Tag: ENV['MANGOPAY_TAG']
      })
      # payment = MangoPay::PayIn::Card::Web.create({
      #   AuthorId: ENV['AUTHOR_ID'],
      #   CreditedUserId: ENV['CREDITED_USER_ID'],
      #   DebitedFunds: { Currency: ENV['CURRENCY'], Amount: trip_items.sum(:price).to_i },
      #   Fees: { Currency: ENV['CURRENCY'], Amount: 0 },
      #   CreditedWalletId: ENV['CREDITED_WALLET_ID'],
      #   ReturnURL: "#{url}/reservation_packages/#{rp.id}/reservations",
      #   CardType: ENV['CARD_TYPE'],
      #   Culture: ENV['CULTURE'],
      #   Tag: ENV['MANGOPAY_TAG']
      # })
      
      rp.create_payment(user, payment)
    end
    payment
  end
  
  def total_amount
    trip_items = TripItem.where("trip_destination_id IN (?)", trip_destinations.pluck(:id))
    trip_items.sum(:price).to_i
  end

end
