class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :listings, dependent: :destroy
  has_many :businesses, dependent: :destroy
  has_many :payments, dependent: :destroy
  has_many :trips, dependent: :destroy
  
  has_attached_file :avatar, styles: { navbar: 'x120', regular: '800x' },
                    default_url: 'missing.png'
  validates_attachment :avatar,
                       content_type: {
                         content_type:
                           [ 'image/jpeg', 'image/jpg', 'image/png', 'image/gif'] },
                       size: { in: 0..5.megabytes }

  validates :is_business, inclusion: { in: [true, false] },
            unless: Proc.new { |u| u.persisted? }

  validates :first_name, presence: true, length: { maximum: 50 },
            unless: Proc.new { |u| u.is_business }
  validates :last_name, presence: true, length: { maximum: 50 },
            unless: Proc.new { |u| u.is_business }
  validates :company_name, presence: true, length: { maximum: 100 },
            if: Proc.new { |u| u.is_business }

  validates :oib, numericality: { only_integer: true,
                                  allow_nil: true,
                                  greater_than: 10000000000,
                                  less_than:    99999999999 }
  validates :gender, inclusion: { in: ['Male', 'Female', 'Other'],
                                  message: "%{value} is not a valid gender" },
            unless: Proc.new { |u| u.is_business or not u.persisted? }
            
  has_one :address, as: :addressable, dependent: :delete
  
  attr_accessor :street_address, :city_id, :postal_code

  def short_name
    is_business ? company_name : first_name
  end

  def full_name
    is_business ? company_name : "#{self.first_name} #{self.last_name}"
  end
end
