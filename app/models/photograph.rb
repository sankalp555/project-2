class Photograph < ActiveRecord::Base
  belongs_to :presentable, polymorphic: true

  has_attached_file :photograph, styles: lambda { |a| a.instance.is_image? ? { thumb: 'x120', regular: '800x', big: '2500x' } : {} }
  validates_attachment :photograph,
                       content_type: {
                         content_type: [ 'image/jpeg', 'image/jpg', 'application/pdf' ]}
#                       size: { in: 0..5.megabytes }
  def is_image?
    return false unless photograph.content_type
    ['image/jpeg', 'image/jpg'].include?(photograph.content_type)
  end
end
