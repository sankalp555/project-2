class Payment < ActiveRecord::Base
  has_one :invoice
  belongs_to :reservation_package
  belongs_to :user

  def package_date_range
  	from = reservation_package.date_from.strftime('%B %d %Y') rescue ""
  	to = reservation_package.date_to.strftime('%B %d %Y') rescue ""
  	[from, to].reject { |c| c.empty? }.join(" TO ")
  end
end
