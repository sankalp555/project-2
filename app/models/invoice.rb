class Invoice < ActiveRecord::Base
  has_many :photographs, as: :presentable, dependent: :destroy
  has_many :invoice_lines, dependent: :destroy
  belongs_to :payment

  def formatted_created_at
  	created_at.strftime('%d %B %Y')
  end
end
