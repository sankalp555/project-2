class CardRegistrationService
  require 'net/http'
  
  def initialize(trip_id,user,root_url)
    @trip = Trip.find trip_id
    @user = user
    @root_url = root_url
  end
  
  def initiate_card_registration(user_card)
    create_card = create
    authenticate_card_data = authenticate_card_info(create_card,user_card)
    update_card = update(create_card["Id"],authenticate_card_data)
    payment_response = create_pre_authrization(update_card["CardId"])
    return payment_response
  end

  def create
    init_owner_details = MangoPay::CardRegistration.create({ "UserId": ENV['AUTHOR_ID'],"Currency": "EUR","CardType": "CB_VISA_MASTERCARD"})
  end

  def authenticate_card_info(auth_details,user_card)
    access_key = auth_details["AccessKey"]
    pre_reg_data = auth_details["PreregistrationData"]
    card_number = user_card[:card_number]
    card_month = user_card[:month]
    card_year = user_card[:year]
    card_cvv = user_card[:cvv]
    url = URI(auth_details["CardRegistrationURL"])
    http = Net::HTTP.new(url.host, url.port)
    http.use_ssl = true
    request = Net::HTTP::Post.new(url)
    request["Content-Type"] = 'application/x-www-form-urlencoded'
    request["cache-control"] = 'no-cache'
    request.body = "accessKeyRef="+access_key+"&data="+pre_reg_data+"&cardNumber="+card_number+"&cardExpirationDate="+card_month+card_year+"&CardCvx="+card_cvv
    response = http.request(request)
    data = response.read_body
  end

  def update(id,data_to_update)
    update_card_details = MangoPay::CardRegistration.update(id,{RegistrationData: data_to_update})
  end

  def create_pre_authrization(card_id)
    pre_auth_response = @trip.create_reservation_package(@user, @root_url, card_id)
  end
end