# coding: utf-8
# data for planner session is kept in cookies
# TODO setup memcached session storage

class PlannerSessionService
  PlannerSession = Struct.new(:destinations,
                              :reservations,
                              :selected_date,
                              :selected_destination,
                              :selected_ts_range)
  Destination = Struct.new(:location, :ts_range, :person_count)

  class ReservationProxy
    attr_accessor :reservable_type, :id, :price, :ts_range, :destination

    def initialize(reservable_type, id, price, ts_range, destination)
      @reservable_type = reservable_type
      @id = id
      @price = price
      @ts_range = ts_range
      @destination = destination
    end

    def get_reservable
      @reservable_type.constantize.find @id
    end

    def get_reservation
      Reservation.new(status: 0,
                      date_from: ts_range.first,
                      date_to: ts_range.last,
                      reservable: get_reservable)
    end
  end
 

  def initialize(planner_session_cookies, user)
    @current_user = user

    begin
      @planner_data = Marshal.load planner_session_cookies
    rescue
      new_session
    end
  end

  def new_session
    @planner_data = PlannerSession.new(
      Array.new,
      Array.new,
      nil
    )
  end

  def session_defined?
    not (@planner_data.destinations.empty? and @planner_data.reservations.empty?)
  end

  def set_selected_date_destination(date, destination)
    @planner_data.selected_date = date
    @planner_data.selected_destination = destination
    @planner_data.selected_ts_range = get_ts_range_for(date, destination)
  end

  def selected
    { date: @planner_data.selected_date,
      destination: @planner_data.selected_destination }
  end

  def selected_date
    @planner_data.selected_date
  end

  def selected_destination
    @planner_data.selected_destination
  end

  def all_dates_destinations
    @planner_data.destinations.collect_concat do |d|
      dates = d.ts_range.to_a

      dates.each.collect_concat do |date|
        { date: date, destination: d[:location] }
      end
    end.sort do |a, b|
      a[:date] <=> b[:date]
    end
  end

  def selected_ts_range
    @planner_data.selected_ts_range
  end

  def get_ts_range_for(date, destination)
    @planner_data.destinations.find do |d|
      d.ts_range.include? date and d.location == destination
    end[:ts_range]
  end

  # TODO check if ts_ranges overlap; Rails has overlaps? method defined
  # for Range class
  def add_destination(location, start_date, end_date, person_count)
    ts_range = Date.parse(start_date)..Date.parse(end_date)
    @planner_data.destinations << Destination.new(location, ts_range, person_count)
  end

  def add_accommodation(id, destination, ts_range, type)
    dates = ts_range.split("..")
    if type == 'Lighthouse'
      price = ReservablePriceQuery.new(Lighthouse.find(id)).get_price(dates[0].to_date..dates[1].to_date)
      @planner_data.reservations << ReservationProxy.new('Lighthouse', id, price, dates[0].to_date..dates[1].to_date, destination)
    elsif type == 'Island'
      price = ReservablePriceQuery.new(Island.find(id)).get_price(dates[0].to_date..dates[1].to_date)
      @planner_data.reservations << ReservationProxy.new('Island', id, price, dates[0].to_date..dates[1].to_date, destination)
    elsif type == 'Charter'
      price = ReservablePriceQuery.new(Charter.find(id)).get_price(dates[0].to_date..dates[1].to_date)
      @planner_data.reservations << ReservationProxy.new('Charter', id, price, dates[0].to_date..dates[1].to_date, destination)  
    else
      price = ReservablePriceQuery.new(Room.find(id)).get_price(dates[0].to_date..dates[1].to_date)
      @planner_data.reservations << ReservationProxy.new('Room', id, price, dates[0].to_date..dates[1].to_date, destination)
    end
  end

  def get_accommodation
    @planner_data.reservations.find do |r|
      r.reservable_type == 'Room' || r.reservable_type == 'Island' || r.reservable_type == 'Lighthouse'  || r.reservable_type == 'Charter' &&
        r.ts_range.include?(@planner_data.selected_ts_range)
    end
  end

  def remove_accommodation(ts_range, destination)
		split_date = ts_range.split('..')
    @planner_data.reservations.delete_if do |r| 
			r.reservable_type == 'Room' || r.reservable_type == 'Island' || r.reservable_type == 'Lighthouse'  || r.reservable_type == 'Charter' && r.destination == destination && r.ts_range.first.to_date == split_date[0].to_date && r.ts_range.last.to_date == split_date[1].to_date
		end
  end

  def add_activity(type, id, destination, date)
    activity = type.constantize.find(id)
    price = ReservablePriceQuery.new(activity).get_price(date.to_date..date.to_date)
    @planner_data.reservations <<
      ReservationProxy.new(
      type,
      id,
      price,
      date.to_date..date.to_date,
      destination)
  end

  def get_activities
    @planner_data.reservations.find_all do |r|
      ['Island', 'Lighthouse', 'Room', 'Charter'].exclude?(r.reservable_type) && @planner_data.selected_date
    end
  end

  def remove_activity(type, id, ts_range, destination)
    split_date = ts_range.split('..')
    @planner_data.reservations.delete_if do |r|
      r.reservable_type == type && r.id == id && r.destination == destination && r.ts_range.first.to_date == split_date[0].to_date && r.ts_range.last.to_date == split_date[1].to_date
    end
  end

  def book
    all_dates = all_dates_destinations.collect_concat { |el| el[:date] }

    rp = ReservationPackage.new(user: @current_user,
                                date_from: all_dates.first,
                                date_to: all_dates.last)

    @planner_data.reservations.each do |proxy|
      rp.reservations << proxy.get_reservation
    end

    rp.save

    return rp
  end

  def no_reservations?
    @planner_data.reservations.empty?
  end

  def get_price
    if no_reservations?
      return 'Planner is empty'
    end

    '€' + @planner_data.reservations.inject(0) do |memo, el|
      memo + el.price
    end.to_s
  end

  def to_cookies_field
    Marshal.dump @planner_data
  end
end
