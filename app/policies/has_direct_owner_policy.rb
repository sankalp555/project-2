class HasDirectOwnerPolicy
  attr_reader :user, :resource
  
  def initialize(user, resource)
    @user = user
    @resource = resource
  end

  def show?
    true
  end

  def edit?
    @user == @resource.owner
  end
  
  def update?
    @user == @resource.owner
  end
  
  def delete?
    @user == @resource.owner
  end
  
  def destroy?
    @user == @resource.owner
  end
end
