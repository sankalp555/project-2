class RoomPolicy < HasDirectOwnerPolicy
  def update?
    is_valid?
  end

  def edit?
    is_valid?
  end

  def destroy?
    is_valid?
  end
  
  def is_valid?
    @user.is_business? && (@resource.owner.id rescue @resource.lodging_provider.user_id)
  end
end