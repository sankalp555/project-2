class PhotographPolicy
  attr_reader :user, :resource
  
  def initialize(user, resource)
    @user = user
    @resource = resource
  end

  def create?
    check
  end

  def update?
    check
  end
  
  def destroy?
    check
  end

  private

  def check
    return @user == @resource.presentable.owner if @resource.presentable.try(:owner)
    return @user == @resource.presentable.rent.owner if @resource.presentable.try(:rent)
    return @user == @resource.presentable.room.owner if @resource.presentable.try(:room)
    return PrivateAccommodation.find(@resource.presentable.lodging_provider_id).user_id == @user.id
    return false
  end
end
