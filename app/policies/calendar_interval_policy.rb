class CalendarIntervalPolicy
  attr_reader :user, :resource
  
  def initialize(user, resource)
    @user = user
    @resource = resource.scheduable
  end

  def create?
    is_valid?
  end

  def is_valid?
    return @user == @resource.owner if @resource.try(:owner)
    return @user == @resource.rent.owner if @resource.try(:rent)
    return @user.id == @resource.lodging_provider.user_id
  end
end
