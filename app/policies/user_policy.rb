class UserPolicy
  attr_reader :user, :resource
  
  def initialize(user, resource)
    @user = user
    @resource = resource
  end

  def edit?
    @user == @resource
  end
  
  def update?
    @user == @resource
  end
  
  def delete?
    @user == @resource
  end
  
  def destroy?
    @user == @resource
  end
end