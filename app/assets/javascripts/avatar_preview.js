var show_avatar = function () {
    $('input[type=file]#user_avatar').change(function (e) {
	var files = e.target.files
	$('div#avatar_preview').empty();

	for (var i = 0; i < files.length; ++i) {
	    var img = document.createElement('img');
	    
	    img.file = files[i];
	    $('div#avatar_preview').append(img);

	    var reader = new FileReader();
	    reader.onload = (function (aImg) {
		return function (e) {
		    aImg.src = e.target.result;
		};
	    })(img);

	    reader.readAsDataURL(e.target.files[i]);
	}
    });
};
