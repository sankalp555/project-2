// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery-ujs
//= require jquery-ui
//= require autocomplete-rails
//= require jquery-ui/ui/widgets/datepicker
//= require knockoutjs
//= require moment
//= require fullcalendar
//= require leaflet
//= require leaflet.markercluster
//= require avatar_preview
//= require gallery
//= require listing_map_pin_placer
//= require rent_address_setter
//= require planner
//= require new_planner
//= require setup_daterange_picker
//= require cocoon
//= require jquery.smoothwheel

$.datepicker.setDefaults({
    dateFormat: "yy-mm-dd",
    firstDay: 1
});

  var slideIndex = 1;
  showSlides(slideIndex);

  function plusSlides(n) {
    showSlides(slideIndex += n);
  }

  function currentSlide(n) {
    showSlides(slideIndex = n);
  }

  function showSlides(n) {
    var i;
    var slides = document.getElementsByClassName("mySlides");
    //var dots = document.getElementsByClassName("dot");
    if (n > slides.length) {slideIndex = 1}    
    if (n < 1) {slideIndex = slides.length}
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";  
    }
    // for (i = 0; i < dots.length; i++) {
    //     dots[i].className = dots[i].className.replace(" active", "");
    // }
    if (slides.length != 0) { slides[slideIndex-1].style.display = "block"; }  
    // dots[slideIndex-1].className += " active";
  }
