var gallery = function(imageAddEl, galleryViewEl, formImageCounter, photoUrls,
		       afterUploadFunction)
{
    function Image(id, original_id, src, seqNo, isNew, toDelete, postUrl) {
	this.id = id;
	this.original_id = original_id;
	this.src = src;
	this.seqNo = seqNo;
	this.isNew = isNew;
	this.moved = false;
	this.toDelete = toDelete;
	this.postUrl = postUrl;
    }

    Image.prototype.increaseSeqNo = function () {
	this.seqNo++;
	this.moved = true;
    };

    Image.prototype.decreaseSeqNo = function () {
	this.seqNo--;
	this.moved = true;
    };
 
    function ImageView(imageModel) {
	this.model = imageModel;
	var img = document.createElement("img");
	var wrapper = $('<span class="img-wrapper"></span>');
	wrapper.html(img);

	if (imageModel.src instanceof File) { 
	    var reader = new FileReader();
	    reader.onload = (function(aImg) {
		return function(e) { aImg.src = e.target.result; };
	    })(img);

	    reader.readAsDataURL(imageModel.src);
	}
	else { //imageModel.src is url, can be loaded directly
	    img.src = imageModel.src;
	}


	var del = $('<span class="image-delete"><i class="fa fa-trash"></i></span>');
	var left = $('<span class="image-move-left"><i class="fa fa-angle-left"></i></span>'); 
	var right = $('<span class="image-move-right"><i class="fa fa-angle-right"></i></span>');
	wrapper.prepend(del);
	wrapper.prepend(left);
	wrapper.prepend(right);

	del.on('click', function(e) {
	    controller.removeImage(this.model, this);
	    this.html.remove();
	}.bind(this));

	left.on('click', function(e) {
	    controller.moveLeft(this.model, this);
	}.bind(this));

	right.on('click', function(e) {
	    controller.moveRight(this.model, this);
	}.bind(this));

	this.html = wrapper;
    }

    ImageView.prototype.getModelId = function() {
	return this.model.id;
    };

    ImageView.prototype.getHtml = function () {
	return this.html;
    };

    var imagesRepository = (function() {
	var images = new Array();
	var id = 1;

	var size = function() {
	    return images.reduce(function (previous, current, index, array) {
		if (!current.toDelete) {
		    return previous + 1;
		} else {
		    return previous;
		}
	    }, 0);
	};

	var getLargestSeqNo = function() {
	    var largest = 0;

	    for (var i = 0, length = images.length; i < length; ++i) {
		if (images[i].seqNo > largest)
		    largest = images[i].seqNo;
	    }

	    return largest;
	};

	var getNewImagesFormDataArray = function() {
	    images.sort(function(a, b) {
		if (a.seqNo > b.seqNo)
		    return 1;
		if (a.seqNo < b.seqNo)
		    return -1;
		return 0;
	    });
	    
	    var formDataArray = images.filter(function(element, index, array) {
		return element.isNew === true && element.toDelete === false;
	    }).map(function(element, index, array) {
		var formData = new FormData();

		formData.append('photograph', element.src);
		formData.append('sequence_number', element.seqNo);

		return formData;
	    });

	    return formDataArray;
	};

	var getMovedImages = function() {
	    return images.filter(function(element, index, array) {
		return element.moved;
	    });
	};

	var getDeletedImages = function () {
	    return images.filter(function(element, index, array) {
		return element.isNew === false && element.toDelete === true;
	    });
	};

	var createFromFile = function(imageSrc) {
	    if (!(imageSrc instanceof File))
		throw 'Argument error';
	    
	    var imageType = /^image\//;
	    
	    if (!imageType.test(imageSrc.type)) {
		throw 'Invalid image type';
	    }

	    var seqNo = getLargestSeqNo() + 1;
	    var image = new Image(id, undefined, imageSrc, seqNo, true, false, '');
	    images.push(image);
	    ++id;
	    return image;
	};

	var createFromUrl = function(original_id, image_url, seq_no, post_url) {
	    var image = new Image(id, original_id, image_url, seq_no, false, false, post_url);
	    images.push(image);
	    ++id;
	    return image;
	};


	var remove = function(id) {
	    image = find(id);
	    image.toDelete = true;
	    var seqNo = image.seqNo;

	    // lower seqNo of each image with seqNo larger than deleted images'
	    images.forEach(function(element, index, array) {
		if (element.seqNo > seqNo)
		    element.decreaseSeqNo();
	    });
	};

	var find = function(id) {
	    for (var i = 0, length = images.length; i < length; ++i)
		if (images[i].id === id)
		    return images[i];

	    return undefined;
	};

	var findBySeqNo = function(seqNo) {
	    for (var i = 0, length = images.length; i < length; ++i)
		if (images[i].seqNo === seqNo && !images[i].toDelete)
		    return images[i];

	    return undefined;
	};

	return {
	    size: size,
	    getNewImagesFormDataArray: getNewImagesFormDataArray,
	    getMovedImages: getMovedImages,
	    getDeletedImages: getDeletedImages,
	    createFromFile: createFromFile,
	    createFromUrl: createFromUrl,
	    find: find,
	    findBySeqNo: findBySeqNo,
	    remove: remove
	};
    })();

    var controller = (function (imageAddEl, galleryViewEl, formImageCounter,
			       preexistingPhotos) {
	var createView = function(image) {
	    var view = new ImageView(image);
	    imageViews.push(view);
	    galleryViewEl.append(view.getHtml());
	    console.log('new views', imageViews);
	};

	var removeImage = function(model) {
	    imagesRepository.remove(model.id);
	    formImageCounter.val(imagesRepository.size());

	    var index = findImageViewIndex(model.id);
	    imageViews.splice(index, 1);
	};

	var moveLeft = function(model) {
	    if (imagesRepository.size() < 2)
		return; // less than 2 elements, nothing to do
	    
	    if (model.seqNo === 1)
		return; // leftmost element, nothing to do

	    var model_left = model;
	    var model_right = imagesRepository.findBySeqNo(model.seqNo - 1);
	    model_left.decreaseSeqNo();
	    model_right.increaseSeqNo();

	    // swap views
	    var index_left = findImageViewIndex(model_left.id);
	    var index_right = findImageViewIndex(model_right.id);

	    var tmp = imageViews[index_right];
	    imageViews[index_right] = imageViews[index_left];
	    imageViews[index_left] = tmp;

	    galleryViewEl.children().detach();

	    imageViews.forEach(function(item, index, array) {
		galleryViewEl.append(item.getHtml());
	    });
	};

	var moveRight = function(model) {
	    if (imagesRepository.size() < 2)
		return; // less than 2 elements, nothing to do
	    
	    if (model.seqNo === imagesRepository.size())
		return; // rightmost element, nothing to do

	    var model_right = model;
	    var model_left = imagesRepository.findBySeqNo(model_right.seqNo + 1);
	    model_left.decreaseSeqNo();
	    model_right.increaseSeqNo();

	    // swap views
	    var index_left = findImageViewIndex(model_left.id);
	    var index_right = findImageViewIndex(model_right.id);

	    var tmp = imageViews[index_right];
	    imageViews[index_right] = imageViews[index_left];
	    imageViews[index_left] = tmp;

	    galleryViewEl.children().detach();

	    imageViews.forEach(function(item, index, array) {
		galleryViewEl.append(item.getHtml());
	    });
	};

	var findImageViewIndex = function(modelId) {
	    var index = undefined;

	    for (var i = 0, length = imageViews.length; i < length; ++i)
		if (imageViews[i].getModelId() === modelId)
		    index = i;

	    return index;
	};

	var upload_completed = function () {
	    window.eval(afterUploadFunction);
	};

	var upload = function(uploadUrl) {
	    var el = $('form input[type=submit]');

	    var new_photos = imagesRepository.getNewImagesFormDataArray();
	    var moved_photos = imagesRepository.getMovedImages();
	    var deleted_photos = imagesRepository.getDeletedImages();

	    var remainingPhotos = new_photos.length +
		moved_photos.length + deleted_photos.length;
	    var count = remainingPhotos;

	    if (remainingPhotos === 0)
		upload_completed();

	    new_photos.forEach(function(element, index, array) {
		var request = new XMLHttpRequest();
		var progress_wrapper = $('<div class="progress-wrapper">Uploading image ' + (index+1) + '/' + count + ': </div>');
		var progress_bar = $('<progress value="0" max="1"></progress>');
		var progress_text = $('<span class="progress-text"></span>');

		progress_wrapper.append(progress_bar);
		progress_wrapper.append(progress_text);
		el.before(progress_wrapper);

		request.upload.addEventListener('progress', function(e) {
		    var ratio = e.loaded / e.total;

		    if (ratio < 1) {
			progress_bar.attr('value', ratio);
		    } else {
			progress_bar.attr('value', 1);
			progress_text.text('Processing image...');
		    }
		});
		request.addEventListener('loadend', function(e) {
		    progress_text.text('Done');
		    --remainingPhotos;

		    if (remainingPhotos === 0)
			upload_completed();
		});

		request.open('POST', uploadUrl);
		setRequestHeaders(request);

		request.send(element);
	    });

	    moved_photos.forEach(function (element, index, array) {
		var form_data = new FormData();
		var request = new XMLHttpRequest();

		form_data.append('sequence_number', element.seqNo);

		request.open('PATCH', element.postUrl);
		setRequestHeaders(request);

		request.addEventListener('loadend', function(e) {
		    --remainingPhotos;

		    if (remainingPhotos === 0)
			upload_completed();
		});

		request.send(form_data);
	    });

	    deleted_photos.forEach(function (element, index, array) {
		var request = new XMLHttpRequest();

		request.open('DELETE', element.postUrl);
		setRequestHeaders(request);

		request.addEventListener('loadend', function(e) {
		    --remainingPhotos;

		    if (remainingPhotos === 0)
			upload_completed();
		});

		request.send();
	    });
	};

	var setRequestHeaders = function(request) {
	    request.setRequestHeader('X-CSRF-Token',
				     $('meta[name="csrf-token"]')
				     .attr('content'));
	    request.setRequestHeader('accept',
				     '*/*;q=0.5, text/javascript, application/javascript, application/ecmascript, application/x-ecmascript');
	    request.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
	};

	var loadPreexistingPhotos = function() {
	    preexistingPhotos.forEach(function(element, index, array) {
		var image = imagesRepository.createFromUrl(element.id, element.url, element.seq_no, element.post_url);
		createView(image);
	    });
	};

	// initialization
	var imageViews = new Array();
	var fileElem =
	    $('<input type="file" multiple="true" accept="image/jpeg">');
	
	fileElem.on('change', function (e) {
	    for (var i = 0, length = fileElem.prop('files').length; i < length; ++i) {
		var image = imagesRepository.createFromFile(
		    fileElem.prop('files').item(i));
		createView(image);
	    }

	    formImageCounter.val(imagesRepository.size());
	    return false;
	});			

	imageAddEl.on('click', function() {
      fileElem.trigger('click');

	    return false;
	});

	loadPreexistingPhotos();
	formImageCounter.val(imagesRepository.size());

	return {
	    removeImage: removeImage,
	    moveLeft: moveLeft,
	    moveRight: moveRight,
	    upload: upload };
    })(imageAddEl, galleryViewEl, formImageCounter, photoUrls);

    return { upload: controller.upload };
};
