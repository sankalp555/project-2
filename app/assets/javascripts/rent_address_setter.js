var address_setter = function(map_handler, street_address, postal_code,
			      address_locality, longitude, latitude) {
    model_type = ['car', 'bike', 'boat', 'motorbike'];

    for (var i = 0, length = model_type.length; i < length; ++i) {
	$('#' + model_type[i] + '_address_attributes_street_address').val(street_address);
	$('#' + model_type[i] + '_address_attributes_postal_code').val(postal_code);
	$('#' + model_type[i] + '_address_attributes_address_locality').val(address_locality);
	$('#' + model_type[i] + '_address_attributes_longitude').val(longitude);
	$('#' + model_type[i] + '_address_attributes_latitude').val(latitude);
    }

    map_handler.remove_marker();
    map_handler.add_marker(latitude, longitude);
};

/* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */
function myFunction() {
  document.getElementById("myDropdown").classList.toggle("show");
}

// Close the dropdown if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {
    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}