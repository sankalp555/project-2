"use strict";
var Croffers = Croffers || {};

Croffers.setup_daterange_picker =
    function(start_date_el, end_date_el, min_date, start_date, end_date) {

        start_date_el.datepicker({ defaultDate: start_date,
                                   minDate: min_date });
        end_date_el.datepicker({ defaultDate: end_date });

        start_date_el.change(function () {
            var new_start_date = moment(start_date_el.datepicker('getDate'));
            new_start_date = new_start_date.add(1, 'days');
            new_start_date = new_start_date.format('YYYY-MM-DD');

            end_date_el.datepicker('option', 'minDate', new_start_date);
        });
    };

Croffers.teardown_daterange_picker =
    function(start_date_el, end_date_el) {
	start_date_el.datepicker("destroy");
	end_date_el.datepicker("destroy");
    };
