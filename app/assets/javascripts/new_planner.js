if($("ul #date_list").last())
{
  $('#down_arrow').css('display','block');
}
$('#show_accomodation_details').click(function()
{
  $('#accommodation_right').css('display','block');
  $('.activity_right_Event').css('display','none');
  $('.activity_right_Vehicle').css('display','none');
  $('.activity_right_Activity').css('display','none');
});
$('#show_activity_details').click(function()
{
  $('#accommodation_right').css('display','none');
  $('.activity_right_Event').css('display','none');
  $('.activity_right_Vehicle').css('display','none');
  id = $('#show_activity_details').data('id');
  $('#activity_right_Activity'+id).css('display','block');
});
$('#show_event_details').click(function()
{
  $('#accommodation_right').css('display','none');
  $('.activity_right_Vehicle').css('display','none');
  $('.activity_right_Activity').css('display','none');
  id = $('#show_event_details').data('id');
  type = $('#show_event_details').data('type');
  $('#activity_right_Event'+id).css('display','block');
});
$('#show_vehicle_details').click(function()
{
  $('#accommodation_right').css('display','none');
  $('.activity_right_Event').css('display','none');
  $('.activity_right_Activity').css('display','none');
  id = $('#show_vehicle_details').data('id');
  type = $('#show_vehicle_details').data('type');
  $('#activity_right_Vehicle'+id).css('display','block');
});