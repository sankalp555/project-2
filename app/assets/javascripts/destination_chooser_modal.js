'use strict';

var Croffers = Croffers || {};

Croffers.DestinationChooserVM = function() {
    var self = this;
    var fmt = 'YYYY-MM-DD';

    self.visible = ko.observable(false);
    self.start_date = ko.observable(null);
    self.end_date = ko.observable(null);
    self.destination = ko.observable(null);
    self.person_count = ko.observable(null);
    self.chosen = ko.observableArray([]);
    self.available_start_dates = ko.observableArray([]);
    self.error_messages = ko.observableArray([]);

    self.showDestinationChooser = function() {
        self.visible(true);
    };

    self.hideDestinationChooser = function() {
	if (Croffers.session_defined) {
            self.visible(false);
	}
    };

    /*
     * Option 1: datepicker on both fields
     * Option 2: dropdown list with two dates on first,
     *           datepicker on second field
     */
    self.setupDatepickers = function() {
	var start_date_el = $('#start-date');
	var end_date_el = $('#end-date');

	if (!self.chosenExists()) {
	    var today = moment().format(fmt);
	    var tomorrow = moment().add(1, 'days').format(fmt);

	    start_date_el.removeClass('hasDatepicker');
	    end_date_el.removeClass('hasDatepicker');

	    Croffers.setup_daterange_picker(start_date_el,
					    end_date_el,
					    today,
					    today,
					    tomorrow);
	} else {
	    var start_date = self.chosen().slice(-1)[0].end_date;
	    var next_date = moment(start_date).add(1, 'days').format(fmt);
	    start_date_el.datepicker({ defaultDate: start_date, minDate: start_date });
	    end_date_el.datepicker({ defaultDate: next_date, minDate: next_date });
	    self.start_date(start_date)
      end_date_el.datepicker('option', 'minDate', start_date);
      start_date_el.datepicker('option', 'minDate', start_date);

      start_date_el.change(function () {
          var new_start_date = moment(start_date_el.datepicker('getDate')).add(1, 'days').format('YYYY-MM-DD');
          end_date_el.datepicker('option', 'minDate', new_start_date);
      });
	}
    };

    self.addDestination = function() {
	self.error_messages([]);

	if (self.validate()) {
	    self.chosen.push({
		start_date: self.start_date(),
		end_date: self.end_date(),
		destination: self.destination(),
		person_count: self.person_count()
	    });

	    self.clearForm();
	    
	    return true;
	} else {
		if(self.chosen().length == 0){
			return false;
		}else{
			self.error_messages([]);
	   		window.location.href = '/app/init?' + $.param({ chosen: self.chosen() });
		}
	}
    };

    self.clearDestinations = function() {
	self.chosen([]);
	self.clearForm();
    };

    self.chosenExists = ko.computed(function() {
	return self.chosen().length !== 0;
    }, this);

    self.clearForm = function () {
	self.start_date(null);
	self.end_date(null);
	self.destination(null);
	self.person_count(null);

	self.setupDatepickers();
    };

    self.validate = function() {
	var valid = true;

	if (self.start_date() === null) {
	    self.error_messages.push("Start date not set");
	    valid = false;
	}
	if (self.end_date() === null) {
	    self.error_messages.push("End date not set");
	    valid = false;
	}
	if (self.destination() === null) {
	    self.error_messages.push("Destination not set");
	    valid = false;
	}
	if (self.person_count() === null) {
	    self.error_messages.push("Number of people not specified");
	    valid = false;
	}

	return valid;
    };

    self.submit = function () {
	if (self.addDestination()) {
	    window.location.href =
		'/app/init?' + $.param({ chosen: self.chosen() });
	}
    };

    self.setupDatepickers();

    if (!Croffers.session_defined) {
	self.showDestinationChooser();
    }
};

$(function () {
    ko.applyBindings(new Croffers.DestinationChooserVM());
});
