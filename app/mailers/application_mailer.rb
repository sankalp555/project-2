class ApplicationMailer < ActionMailer::Base
  default from: "croffers@croffers.com"
  layout 'mailer'
end
