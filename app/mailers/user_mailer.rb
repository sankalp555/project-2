require 'open-uri'

class UserMailer < ApplicationMailer
  default from: "Croffers <croffers@croffers.com>"
  helper :application

  def welcome_email (user)
    mail(to: user.email, subject: 'Welcome to Croffers')
  end
  
  def send_invoice(user_id, invoice_id)
    @user = User.find(user_id)
    @invoice = Invoice.find(invoice_id)
    @payment = @invoice.payment
    @reservation_package = @payment.reservation_package
    photograph = @invoice.photographs.last.photograph
    file_url = Rails.env.development? ? photograph.path : photograph.url
    attachments["invoice#{invoice_id}.pdf"] = open(file_url).read
    mail(to: @user.email, subject: "Invoice##{@invoice.id} || Croffers")
  end

  def notify_recevier(message)
    @message = message
    @message_thread = @message.message_thread
    if ['Room'].exclude?(@message_thread.reservation.reservable_type)
      @lodging_provider = @message_thread.reservation.reservable
    else
      @lodging_provider = @message_thread.reservation.reservable.lodging_provider
    end
    @lodging_for = @message_thread.reservation.reservable
    @receiver = @message_thread.receiver
    @sender = @message_thread.sender
    @reservation = @message_thread.reservation
    mail(to: @receiver.email, subject: "New Message || Croffers")
  end

  def notify_providers(obj, user)
    @obj = obj
    @user = user
    mail(to: @user.email, subject: "No availability for your listing || Croffers")
  end

  def weekely_mail(user_id)
    @user = User.find(user_id)
    @trips = @user.trips.draft.order("id asc")
    mail(to: @user.email, subject: "Podsjetnik na čekanju || Croffers")
  end
end
