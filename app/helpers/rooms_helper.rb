# coding: utf-8
module RoomsHelper
def room_amenities
  [ {symbol: :air_conditioning, desc: "Air conditioning", desc_hr: 'Klimatizirano' },
    {symbol: :cleaning_products, desc: "Cleaning products", desc_hr: 'Sredstva za čišćenje' },
    {symbol: :walk_in_closet, desc: "Walk-in closet", desc_hr: 'Ugradbeni ormar' },
    {symbol: :fan, desc: "Fan", desc_hr: 'Ventilator' },
    {symbol: :hot_tub, desc: "Hot tub", desc_hr: 'Kada' },
    {symbol: :ironing, desc: "Ironing facilities", desc_hr: 'Oprema za glačanje' },
    {symbol: :priv_ent, desc: "Private entrance", desc_hr: 'Privatni ulaz' },
    {symbol: :sitting, desc: "Sitting area", desc_hr: 'Prostor za sjedenje' },
    {symbol: :tile_floor, desc: "Tile/marble floor", desc_hr: 'Pločice/mramor na podu' },
    {symbol: :parquet_floor, desc: "Hardwood/parquet floor", desc_hr: 'Parket na podu' },
    {symbol: :hypoallergenic, desc: "Hypoallergenic", desc_hr: 'Antialergijsko' },
    {symbol: :dryer, desc: "Dryer", desc_hr: 'Sušilica' },
    {symbol: :el_blankets, desc: "Electric blankets", desc_hr: 'Električni pokrivači' },
    {symbol: :fireplace, desc: "Fireplace", desc_hr: 'Kamin' },
    {symbol: :interconnecting, desc: "Interconnecting room(s) available", desc_hr: 'Moguće spajanje soba' },
    {symbol: :mosquito, desc: "Mosquito net", desc_hr: 'Mreža protiv komaraca' },
    {symbol: :pool, desc: "Private pool", desc_hr: 'Privatni bazen' },
    {symbol: :sofa, desc: "Sofa", desc_hr: 'Sofa' },
    {symbol: :wardrobe, desc: "Wardrobe/closet", desc_hr: 'Garderoba/ormar' },
    {symbol: :carpeted, desc: "Carpeted", desc_hr: 'Tepih' },
    {symbol: :desk, desc: "Desk", desc_hr: 'Stol' },
    {symbol: :long_beds, desc: "Extra long beds", desc_hr: 'Posebno dugački kreveti' },
    {symbol: :heating, desc: "Heating", desc_hr: 'Grijanje' },
    {symbol: :iron, desc: "Iron", desc_hr: 'Pegla' },
    {symbol: :suit, desc: "Suit press", desc_hr: 'Glačalo za odijelo' },
    {symbol: :safe, desc: "Safe", desc_hr: 'Sef' },
    {symbol: :soundproof, desc: "Soundproof", desc_hr: 'Zvučna izolacija' },
    {symbol: :wash, desc: "Washing machine", desc_hr: 'Perilica rublja' }]
end

def bathroom
  [ {symbol: :additional, desc: "Additional bathroom", desc_hr: 'Dodatna kupaona' },
    {symbol: :bathtub_shower, desc: "Bathtub or shower", desc_hr: 'Kada ili tuš' },
    {symbol: :toiletries, desc: "Free toiletries", desc_hr: 'Besplatni sapun i šampon' },
    {symbol: :shared_bathroom, desc: "Shared bathroom", desc_hr: 'Dijeljena kupaona' },
    {symbol: :slippers, desc: "Slippers", desc_hr: 'Papuče' },
    {symbol: :bathroom, desc: "Bathroom", desc_hr: 'Kupaona' },
    {symbol: :guest_bathroom, desc: "Guest bathroom", desc_hr: 'Kupaona za goste' },
    {symbol: :bathrobe, desc: "Bathrobe", desc_hr: 'Ogrtač' },
    {symbol: :hairdryer, desc: "Hairdryer", desc_hr: 'Sušilo za kosu' },
    {symbol: :shared_toilet, desc: "Shared toilet", desc_hr: 'Dijeljeni zahod' },
    {symbol: :spa_tub, desc: "Spa tub", desc_hr: 'Spa kada' },
    {symbol: :bathtub, desc: "Bathtub", desc_hr: 'Kada' },
    {symbol: :bidet, desc: "Bidet", desc_hr: 'Bidet' },
    {symbol: :sauna, desc: "Sauna", desc_hr: 'Sauna' },
    {symbol: :shower, desc: "Shower", desc_hr: 'Tuš' },
    {symbol: :toilet, desc: "Toilet", desc_hr: 'Zahod' }]
end

def media_technology
  [ {symbol: :blu_ray, desc: "Blu-ray player", desc_hr: 'Blu-ray player' },
    {symbol: :computer, desc: "Computer", desc_hr: 'Računalo' },
    {symbol: :flat_tv, desc: "Flat-screen TV", desc_hr: 'LCD/plazma TV' },
    {symbol: :console, desc: "Game console", desc_hr: 'Igraća konzola' },
    {symbol: :console_ps2, desc: "Game console - PS2", desc_hr: 'Igraća konzola - PS2' },
    {symbol: :console_ps3, desc: "Game console - PS3", desc_hr: 'Igraća konzola - PS2' },
    {symbol: :console_ps4, desc: "Game console - PS4", desc_hr: 'Igraća konzola - PS2' },
    {symbol: :console_wii, desc: "Game console - Wii", desc_hr: 'Igraća konzola - Wii' },
    {symbol: :console_xbox, desc: "Game console - Xbox 360", desc_hr: 'Igraća konzola - Xbox 360' },
    {symbol: :laptop, desc: "Laptop", desc_hr: 'Laptop' },
    {symbol: :radio, desc: "Radio", desc_hr: 'Radio' },
    {symbol: :telephone, desc: "Telephone", desc_hr: 'Telefon' },
    {symbol: :ipad, desc: "iPad", desc_hr: 'iPad' },
    {symbol: :cd_play, desc: "CD player", desc_hr: 'CD player' },
    {symbol: :dvd_play, desc: "DVD player", desc_hr: 'DVD player' },
    {symbol: :laptop_safe, desc: "Laptop safe", desc_hr: 'Sef za laptop' },
    {symbol: :satellite, desc: "Satellite channels", desc_hr: 'Satelitska TV'},
    {symbol: :video, desc: "Video", desc_hr: 'Video' },
    {symbol: :ipod_dock, desc: "iPod dock", desc_hr: 'iPod dock' },
    {symbol: :cable, desc: "Cable channels", desc_hr: 'Kabelska TV' },
    {symbol: :fax, desc: "Fax", desc_hr: 'Fax' },
    {symbol: :pay_channels, desc: "Pay-per-view channels", desc_hr: 'Pay-per-view kanali' },
    {symbol: :tv, desc: "TV", desc_hr: 'TV' },
    {symbol: :video_games, desc: "Video games", desc_hr: 'Video igre' }]
end

def food_drink
  [ {symbol: :barbecue, desc: "Barbecue", desc_hr: 'Roštilj' },
    {symbol: :dining_table, desc: "Dining table", desc_hr: 'Stol za jelo' },
    {symbol: :high_chair, desc: "High chair", desc_hr: 'Dječja stolica' },
    {symbol: :kitchenware, desc: "Kitchenware", desc_hr: 'Posuđe' },
    {symbol: :outdoor_dining, desc: "Outdoor dining area", desc_hr: 'Prostor za jelo na otvorenom' },
    {symbol: :refrigerator, desc: "Refrigerator", desc_hr: 'Hladnjak' },
    {symbol: :toaster, desc: "Toaster", desc_hr: 'Toster' },
    {symbol: :coffee, desc: "Coffee machine", desc_hr: 'Aparat za kavu' },
    {symbol: :dishwasher, desc: "Dishwasher", desc_hr: 'Perilica suđa' },
    {symbol: :kitchen, desc: "Kitchen", desc_hr: 'Kuhinja' },
    {symbol: :microwave, desc: "Microwave", desc_hr: 'Mikrovalna pećnica' },
    {symbol: :outdoor_furniture, desc: "Outdoor furniture", desc_hr: 'Vrtni namještaj' },
    {symbol: :stovetop, desc: "Stovetop", desc_hr: 'Štednjak' },
    {symbol: :dining, desc: "Dining area", desc_hr: 'Prostor za jelo' },
    {symbol: :kettle, desc: "Electric kettle", desc_hr: 'Kuhalo za vodu' },
    {symbol: :kitchenette, desc: "Kitchenette", desc_hr: 'Mini kuhinja' },
    {symbol: :minibar, desc: "Minibar", desc_hr: 'Minibar' },
    {symbol: :oven, desc: "Oven", desc_hr: 'Pećnica' },
    {symbol: :tea_coffee, desc: "Tea/coffee maker", desc_hr: 'Kuhalo za kavu/čaj' }]
end

def services_extras
  [ {symbol: :alarm_clock, desc: "Alarm clock", desc_hr: 'Budilica' },
    {symbol: :towels, desc: "Towels", desc_hr: 'Ručnici' },
    {symbol: :executive, desc: "Executive lounge access", desc_hr: 'Pristup izvršnom salonu' },
    {symbol: :towels_extra, desc: "Towels/sheets (extra fee)", desc_hr: 'Ručnici/plahte (dodatno plaćanje)' },
    {symbol: :linens, desc: "Linens", desc_hr: 'Posteljina' },
    {symbol: :wake_up, desc: "Wake up service", desc_hr: 'Usluga buđenja' }]
end

def outdoor_view
  [ {symbol: :city_view, desc: "City view", desc_hr: 'Pogled na grad' },
    {symbol: :landmark_view, desc: "Landmark view", desc_hr: 'Pogled na značajnu građevinu' },
    {symbol: :pool_view, desc: "Pool view", desc_hr: 'Pogled na bazen' },
    {symbol: :terrace, desc: "Terrace", desc_hr: 'Terasa' },
    {symbol: :garden_view, desc: "Garden view", desc_hr: 'Pogled na vrt' },
    {symbol: :mountain_view, desc: "Mountain view", desc_hr: 'Pogled na planinu' },
    {symbol: :river_view, desc: "River view", desc_hr: 'Pogled na rijeku' },
    {symbol: :view, desc: "View", desc_hr: 'Pogled' },
    {symbol: :lake_view, desc: "Lake view", desc_hr: 'Pogled na jezero' },
    {symbol: :patio, desc: "Patio", desc_hr: 'Terasa' },
    {symbol: :sea_view, desc: "Sea view", desc_hr: 'Pogled na more' },
    {symbol: :balcony, desc: "Balcony", desc_hr: 'Balkon' }]
end
end
