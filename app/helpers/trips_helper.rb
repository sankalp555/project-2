module TripsHelper

  def get_destination(id)
    @destination = TripDestination.find(id)
  end

  def get_accomodation(destination_id)
    @accommodation = TripItem.where("trip_destination_id = ? AND item_type IN (?)", destination_id, ['Room','Island','Lighthouse','Charter']).last
  end

  def get_activities(destination_id)
    @activities = TripItem.where("trip_destination_id = ? AND item_type IN (?)", destination_id, ['Activity','Event','Car','Bike', 'Motorbike', 'Boat'])
  end

  def get_trip_image_name(obj)
    if obj.item_type == 'Activity'
      'icon-excursion.png'
    elsif obj.item_type == 'Event'
      'icon-sport.png'
    else
      'icon-rent.png'
    end
  end

  def get_trip_accom_img(obj)
    if obj.item_type == 'Lighthouse'
      icon = 'icon-lighthouse.png'
    elsif obj.item_type == 'Island'
      icon = 'icon-island.png'
    elsif obj.item_type == 'Charter'
      icon = 'chartered.png'
    elsif obj.item_type == 'Room'
      room = Room.find(obj.item_id)
      icon = 'icon-apartments.png' if room.lodging_provider_type == 'PrivateAccommodation'
      icon = 'icon-hotel.png' if room.lodging_provider_type == 'Hotel'
      icon = 'icon-hostel.png' if room.lodging_provider_type == 'Hostel'
    end
    return icon
  end

  def arranged_trip_activities(activities, d)
    arranged_activities = []
    acts = activities.select{|a| a.item_type == 'Activity' && d[:date] == a.start_date && a.trip_destination.city_id == d[:city_id]}
    events = activities.select{|a| a.item_type == 'Event' && d[:date] == a.start_date && a.trip_destination.city_id == d[:city_id]}
    others = activities.select{|a| ['Activity', 'Event'].exclude?(a.item_type) && d[:date] == a.start_date && a.trip_destination.city_id == d[:city_id]}
    acts.each{|a| arranged_activities.push(a)}
    events.each{|e| arranged_activities.push(e)}
    others.each{|o| arranged_activities.push(o)}
    count = arranged_activities.count
    count < 3 ? (3 - count).to_i.times { arranged_activities.push(nil) } : (count+1 - count).to_i.times { arranged_activities.push(nil) }
    arranged_activities
  end

end
