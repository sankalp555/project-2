module ReservationStatusHelper
  def colored_status_span(reservation)
    if reservation.confirmation_pending?
      "<span style='color: #f0ad4e;'>Pending</span>".html_safe
    elsif reservation.confirmed?
      "<span style='color: #5cb85c;'>#{reservation.status.titlecase}</span>".html_safe
    elsif reservation.rejected?
      "<span style='color: #6c757d;'>#{reservation.status.titlecase}</span>".html_safe
    else
      "<span style='color: #d9534f;'>#{reservation.status.titlecase}</span>".html_safe
    end
  end
end
