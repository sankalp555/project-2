module ApplicationHelper

  def link_to_add_fields(name, f, association)
    new_object = f.object.send(association).klass.new
    id = new_object.object_id
    fields = f.fields_for(association, new_object, child_index: id) do |builder|
      render("shared/" + association.to_s.singularize + "_fields", f: builder)
    end
    link_to(name, '#', class: "add_fields", data: {id: id, fields: fields.gsub("\n", "")})
  end

  def resource_name
    :user
  end
 
  def resource
    @resource ||= User.new
  end
 
  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end

  def listing_show_path(listing)
    if listing.class.name == 'RentBike'
      bike_path(listing)
    elsif listing.class.name == 'RentMotorbike'
      motorbike_path(listing)
    elsif listing.class.name == 'RentCar'
      car_path(listing)
    elsif listing.class.name == 'RentBoat'
      boat_path(listing)
    else
      eval("#{listing.class.name.downcase}_path(listing)")
    end
  end

  def get_calendar_url(obj)
    url = ""
    url = room_calendar_intervals_url(@obj) if obj.class.name == "Room"
    url = activity_calendar_intervals_url(@obj) if obj.class.name == "Activity"
    url = island_calendar_intervals_url(@obj) if obj.class.name == "Island"
    url = vehicle_calendar_intervals_url(@obj) if obj.class.name == "Vehicle"
    url = lighthouse_calendar_intervals_url(@obj) if obj.class.name == "Lighthouse"
    url
  end

  def get_label(val)
    val.to_i.zero? ? 'No' : 'Yes'
  end
  
  def get_label_bool(val)
    val ? 'Yes' : 'No'
  end

  def label_tag(l)
    return "<span style='color: #5cb85c;'>Active</span>".html_safe if l == true
    return "<span style='color: #f0ad4e;'>Pending</span>".html_safe if l == false
  end

end
