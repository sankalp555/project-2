module PlannerHelper
  def calculate_price
    return 30
  end
  
  def get_image_name(obj)
    if obj.reservable_type == 'Activity'
      'icon-excursion.png'
    elsif obj.reservable_type == 'Event'
      'icon-sport.png'
    else
      'icon-rent.png'
    end
  end

  def get_accom_img(obj)
    if obj == 'Lighthouse'
      'icon-lighthouse.png'
    elsif obj == 'PrivateAccommodation'
      'icon-apartments.png'
    elsif obj == 'Hotel'
      'icon-hotel.png'
    elsif obj == 'Hostel'
      'icon-hostel.png'
    elsif obj == 'Island'
      'icon-island.png'
    elsif obj == 'Charter'
      'chartered.png'
    end
  end
  def arranged_activities(activities, d)
    arranged_activities = []
    acts = activities.select{|a| a.reservable_type == 'Activity' && d[:date] == a.ts_range.first && a.city_id == d[:city_id]}
    events = activities.select{|a| a.reservable_type == 'Event' && d[:date] == a.ts_range.first && a.city_id == d[:city_id]}
    others = activities.select{|a| ['Activity', 'Event'].exclude?(a.reservable_type) && d[:date] == a.ts_range.first && a.city_id == d[:city_id]}
    acts.each{|a| arranged_activities.push(a)}
    events.each{|e| arranged_activities.push(e)}
    others.each{|o| arranged_activities.push(o)}
    count = arranged_activities.count
    count < 3 ? (3 - count).to_i.times { arranged_activities.push(nil) } : (count+1 - count).to_i.times { arranged_activities.push(nil) }
    arranged_activities
  end
end
