class ReservablePriceQuery
  def initialize(reservable)
    @reservable = reservable
  end

  def get_price(ts_range)
    # Event is only unschemable reservable
    return @reservable.base_rate if @reservable.class.name == 'Event'
    
    price = 0

    calendar_intervals = @reservable.calendar_intervals.includes('schemable').containing_daterange(ts_range.first, ts_range.last)
    calendar_intervals.each do |ci|
      ci.date_span.each do |day|
        if ts_range.include? day
          if(['Room', 'Boat', 'Activity', 'Car', 'Motorbike', 'Bike', 'Charter'].include?(@reservable.class.name))
            price += ci.schemable.base_rate
          else
            price += ci.schemable.daily_rate
          end
        end
      end
    end

    return price
  end
end
