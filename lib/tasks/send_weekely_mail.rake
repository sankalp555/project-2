namespace :users do
  desc 'Sending weekely mail for drafted trips'
  task :send_weekely_mail => :environment do
    user_ids = Trip.where(active: false).pluck(:user_id).uniq.reject(&:nil?)
    user_ids.each do |user_id|
      UserMailer.weekely_mail(user_id).deliver_now
    end
  end
end