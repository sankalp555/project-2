namespace :user_listings do
  desc 'Notify providers for no availability'
  task :notify_providers => :environment do
		rooms = Room.select{ |r| r.calendar_intervals.blank? }
		activities = Activity.select{ |r| r.calendar_intervals.blank? }
		vehicles = Vehicle.select{ |r| r.calendar_intervals.blank? }
		islands = Island.select{ |r| r.calendar_intervals.blank? }
		lighthouses = Lighthouse.select{ |r| r.calendar_intervals.blank? }

		rooms.each do |room|
			user = room.lodging_provider.business.user rescue nil
			UserMailer.notify_providers(room, user).deliver_now if user.present?
		end

		activities.each do |activity|
			user = activity.business.user rescue nil
			UserMailer.notify_providers(activity, user).deliver_now if user.present?
		end

		vehicles.each do |vehicle|
			user = vehicle.owner rescue nil
			UserMailer.notify_providers(vehicle, user).deliver_now if user.present?
		end
		
		islands.each do |island|
			user = island.owner rescue nil
			UserMailer.notify_providers(island, user).deliver_now if user.present?
		end

		lighthouses.each do |lighthouse|
			user = lighthouse.owner rescue nil
			UserMailer.notify_providers(lighthouse, user).deliver_now if user.present?
		end
  end
end