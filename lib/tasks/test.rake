# get rid of default Rails test tasks (default Rails test framework is
# not used at all in this application)
Rake::Task["test:all:db"].clear
Rake::Task["test:all"].clear
Rake::Task["test:db"].clear
Rake::Task["test"].clear

desc "Run entire test suite"
task test: ['cucumber', 'spec', 'karma'] do
end

desc "Run karma tests"
task :karma do
  root = Rails.application.root.to_s
  `#{root}/node_modules/karma/bin/karma run`
end
