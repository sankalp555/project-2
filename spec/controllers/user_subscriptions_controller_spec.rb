require 'rails_helper'

RSpec.describe UserSubscriptionsController, type: :controller do
  describe 'POST create when successful' do
    it 'should redirect to /signed_up' do
      post 'create', user_subscription: {
             email: 'a@b.com',
             is_business: true }
      expect(response).to redirect_to '/signed_up'
    end
  end

  describe 'POST create with email already in database' do
    it 'should not be redirected' do
      UserSubscription.create(email: 'a@b.com', is_business: true)
      post 'create', user_subscription: {
             email: 'a@b.com',
             is_business: false }
      expect(response).to render_template('static/about')
    end
  end

  describe 'POST create with invalid email' do
    it 'should not be redirected' do
      post 'create', user_subscription: {
             email: 'ab.com',
             is_business: false }
      expect(response).to render_template('static/about')
    end
  end
end
