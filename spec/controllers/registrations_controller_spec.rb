require 'rails_helper'

RSpec.describe Api::V1::RegistrationsController, type: :controller do
  describe 'registration' do
    it 'should return CSRF headers' do
      @request.env["devise.mapping"] = Devise.mappings[:user]

      post :create, { first_name: 'toni',
                      last_name: 'sth',
                      email: 'user@example.com',
                      password: '12345678' }.to_json

      expect(response).to be_success
      expect(response.headers.keys).to include 'X-CSRF-Param'
      expect(response.headers.keys).to include 'X-CSRF-Token'
    end
  end
end
