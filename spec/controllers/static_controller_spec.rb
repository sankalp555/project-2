require 'spec_helper'

RSpec.describe StaticController, type: :controller do
  describe 'when called without argument' do
    it 'should render static/index.html.erb' do
      get :index

      expect(response).to render_template 'static/index'
    end
  end

  describe 'when called as static_controller#about' do
    it 'should render static/about.html.erb' do
      get :about

      expect(response).to render_template 'static/about'
    end
  end

  describe 'when called as static_controller#business' do
    it 'should render static/business.html.erb' do
      get :business

      expect(response).to render_template 'static/business'
    end
  end

  describe 'when called as static_controller#signed_up' do
    it 'should render static/signed_up.html.erb' do
      get :signed_up

      expect(response).to render_template 'static/signed_up'
    end
  end
end
