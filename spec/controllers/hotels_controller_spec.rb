require 'rails_helper'

RSpec.describe Api::V1::HotelsController, type: :controller do
  describe 'GET request for single resouce' do
    it 'should return JSON API formatted response' do
      @hotel = Hotel.create!(
        user: User.create!(
        first_name: 'daf', last_name: 'fjj',
        email: 't@c.som', password: '11111111111'),
        name: "Adriana",
        description: "Hvar yacht harbour hotel",
        check_in: '11:00',
        check_out: '13:00'
      )

      get :show, id: @hotel.id

      expect(response.content_type).to eq('application/vnd.api+json')
    end
  end
end
