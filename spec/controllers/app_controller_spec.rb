require 'rails_helper'

RSpec.describe AppController, type: :controller do
  describe 'index action' do
    it 'should be protected by basic HTTP authentication' do
      get :index
      expect(response).to have_http_status(:unauthorized)
    end

    it 'should allow access when provided with valid username/password' do
      request.env['HTTP_AUTHORIZATION'] =
        ActionController::HttpAuthentication::Basic
        .encode_credentials('cfs', 'sfc')
      get :index
      expect(response).to have_http_status(:ok)
    end

    it 'should render app/index' do
      request.env['HTTP_AUTHORIZATION'] =
        ActionController::HttpAuthentication::Basic
        .encode_credentials('cfs', 'sfc')
      get :index
      expect(response).to render_template 'app/index'
    end
  end
end
