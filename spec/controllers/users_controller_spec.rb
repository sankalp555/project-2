require 'rails_helper'

RSpec.describe Api::V1::UsersController, type: :controller do
  describe 'GET request for single resource' do
    it 'should return JSON API formatted response' do
      @user = User.create!(
        first_name: 'daf', last_name: 'fjj',
        email: 'a@b.com', password: '11111111')

      get :show, id: @user.id

      expect(response.content_type).to eq('application/json')
    end
  end

  describe 'POST request without user being registered' do
    it 'should be rejected' do
      post :update, id: 1, content_type: 'application/json', data: { type: 'users', id: 1, attributes: { first_name: 'John'}}

      expect(response).to have_http_status :unauthorized
    end
  end

  describe 'PUT request with user existing' do
    it 'should be successfull' do
      @user = User.create!(
        first_name: 'df', last_name: 'fj',
        email: 'abc@b.com', password: '11111111')

      post :update, id: @user.id, content_type: 'application/json',
           data: { type: 'users',
                   id: @user.id,
                   attributes: { oib: '1234567891022' }
                 }

      expect(response).to have_http_status :ok
    end
  end
end
