require 'rails_helper'

RSpec.describe Api::V1::SessionsController, type: :controller do
  describe 'sign in action' do
    it 'should return csrf headers' do
      @user = User.create email: 'user@example.com',
                          password: '12345678',
                          first_name: 'djfka',
                          last_name: 'dkfjk'

      @request.env["devise.mapping"] = Devise.mappings[:user]
      @request.params[:email] = @user.email
      @request.params[:password] = @user.password
      get :create

      expect(response).to be_success
      expect(response.headers.keys).to include 'X-CSRF-Param'
      expect(response.headers['X-CSRF-Param']).to eq('authenticity_token')
      expect(response.headers.keys).to include 'X-CSRF-Token'
    end
  end

  describe 'sign out action' do
    it 'should return empty csrf headers' do
      @user = User.create email: 'user@example.com',
                          password: '12345678',
                          first_name: 'abc',
                          last_name: 'bac'

      sign_in @user

      @request.env["devise.mapping"] = Devise.mappings[:user]
      get :destroy

      expect(response).to have_http_status 302
      expect(response.headers.keys).to include 'X-CSRF-Param'
      expect(response.headers.keys).to include 'X-CSRF-Token'
    end

    it 'should return error when user is not signed in' do
      @request.env["devise.mapping"] = Devise.mappings[:user]
      get :destroy

      expect(response).to have_http_status 302
    end
  end
end
