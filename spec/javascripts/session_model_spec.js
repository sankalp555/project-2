describe('Session model', function () {
    beforeAll(function () {
	spyOn($, 'ajax').and.callFake(function(options) {
	    options.success();
	});;
    });

    it('should always send request to server', function () {
	var session = new Croffers.models.session();

	expect(session.isNew()).toBeFalsy();
    });

    it('should send DELETE request to /api/v1/users/login when destroyed', function () {
	var session = new Croffers.models.session();

	session.destroy();
	expect($.ajax).toHaveBeenCalled();
    });
});
