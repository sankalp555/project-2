describe('Main page view', function () {
    beforeAll(function () {
	this.view = new Croffers.views.main_page();
    });

    it('should render subviews of main page view', function () {
	expect($('#planner').html()).not.toBeUndefined();
	expect($('#offers-list').html()).not.toBeUndefined();
    });

    it('should contain references to all its initialized subviews', function () {
	expect(this.view.components.planner).toBeDefined();
	expect(this.view.components.offers_list).toBeDefined();
    });

    it('should clean up entire DOM when "remove" event is triggered', function () {
	this.view.remove();

	expect($('body').find('div').html()).toBeUndefined();
    });
});
