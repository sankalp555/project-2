describe('Navbar view', function () {
    it('should go to /app when logo is clicked', function () {
	var view = new Croffers.views.navbar();
	view.stopListening = function(a, b, c) {};

	Croffers.router = {
	    navigate: function (destination, options) {}
	};
	spyOn(Croffers.router, 'navigate');

	view.$('#logo').click();

	expect(Croffers.router.navigate).toHaveBeenCalledWith(
	    '/', { trigger: true });
	view.remove();
    });

    describe('when user is not logged in', function () {
	beforeAll(function () {
	    this.view = new Croffers.views.navbar();

	    // prevent event listening detachment; does this create memory leaks?
	    this.view.stopListening = function (one, two, three) {};
	});

	it('should change navigation links when user has logged in', function () {
	    Croffers.current_user = new Croffers.models.user({
		email: 'user@example.com',
		first_name: 'Papi',
		last_name: 'Para'
	    });
	    this.view.session_change();
	    expect($('a#signup').html()).toBeUndefined();
	    expect($('a#login').html()).toBeUndefined();
	    delete Croffers.current_user;
	});

	it('should clean after itself', function () {
	    this.view.remove();
	    expect($('body').find('div').html()).toBeUndefined();
	});
    });

    describe('when user is logged in', function () {
	pending();
    });
});
