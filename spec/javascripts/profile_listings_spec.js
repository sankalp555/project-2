describe('Profile listings view', function () {
    beforeAll(function () {
	Croffers.router = {
	    navigate: function (destination, options) {}
	};
	spyOn(Croffers.router, 'navigate');

	this.view = new Croffers.views.profile_listings();
	$('body').append(this.view.render().el);
    });

    it('should have \'Create listing\' link', function () {
	expect(this.view.$('a:contains(Create listing)').html())
	    .not.toBeUndefined();
    });

    it('should load /create_listing when \'Create listing\' is clicked',
       function () {
	   this.view.$('a:contains(Create listing)').click();

	   expect(Croffers.router.navigate).toHaveBeenCalledWith(
	       '/create_listing', { trigger: true});
       });
});
