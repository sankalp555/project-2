describe('Navbar subview for logged out user', function () {
    beforeAll(function () {
	this.view = new Croffers.views.navbar_user_logged_out();
	$('body').append(this.view.render().el);
    });

    it('should have "Sign up" and "Log in" links', function () {
	expect($('a#signup').html()).not.toBeUndefined();
	expect($('a#login').html()).not.toBeUndefined();
    });

    it('should create a #signup-popup when "Sign up" link is clicked',
       function () {
	   $('a#signup').click();
	   expect($('#signup-modal').html()).not.toBeUndefined();

	   // cleanup; close signup popup
	   $('.cell').click();
       });

    afterAll(function () {
	this.view.destroy();
    });
});
