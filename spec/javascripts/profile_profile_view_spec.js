describe('Profile profile view', function () {
    beforeAll(function () {
	SUCCESSFUL_PROFILE_CHANGE_RESPONSE = {
	    email: 'user@example.com',
	    first_name: 'Ivo',
	    last_name: 'Sivo'
	};

	Croffers.current_user = new Croffers.models.user(
	    { email: 'user@example.com',
	      first_name: 'Ivan',
	      last_name: 'Horvat' });

	spyOn($, 'ajax').and.callFake(function (options) {
	    options.success(SUCCESSFUL_PROFILE_CHANGE_RESPONSE);
	});

	this.view = new Croffers.views.profile_profile();
	$('body').append(this.view.render().el);
    });

    it('should display pre-filled profile form', function () {
	expect(this.view.$('form').html()).not.toBeUndefined();
	expect(this.view.$('#email').val()).toEqual('user@example.com');
	expect(this.view.$('#first_name').val()).toEqual('Ivan');
	expect(this.view.$('#last_name').val()).toEqual('Horvat');
    });

    it('should save updated information to the server when "Save changes" is clicked', function () {
	this.view.$('#first_name').val('Ivo');
	this.view.$('#last_name').val('Sivo');

	this.view.$('button[type=submit]').click();

	expect($.ajax).toHaveBeenCalled();
	expect(Croffers.current_user.get('first_name')).toEqual('Ivo');
	expect(Croffers.current_user.get('last_name')).toEqual('Sivo');
    });

    afterAll(function () {
	delete Croffers.current_user;
	this.view.remove();
    });
});
