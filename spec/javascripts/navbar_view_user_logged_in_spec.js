describe('Navbar view with logged in user', function () {
    beforeAll(function () {
	Croffers.current_user = new Croffers.models.user({
	    email: 'user@example.com',
	    first_name: 'sth',
	    last_name: 'hts'
	});

	Croffers.session = new Croffers.models.session();

	Croffers.router = {
	    navigate: function (destination, options) {}
	};
	spyOn(Croffers.router, 'navigate');

	spyOn($, 'ajax').and.callFake(function(options) {
	    delete Croffers.current_user;
	    Croffers.router.navigate('/', { trigger: true });
	});

	this.view = new Croffers.views.navbar_user_logged_in();
	this.view.render();
    });

    it('should have a name of user present', function () {
	expect(this.view.$el.html()).toMatch(/sth/);
    });

    it('should load profile page when name is clicked', function () {
	this.view.$('a#navbar-dropdown').click();

	expect(Croffers.router.navigate).toHaveBeenCalledWith(
	    '/profile', { trigger: true });
    });

    it('should have a log out option present', function () {
	expect(this.view.$('a#logout').html()).toBeDefined();
    });

    describe('when a#logout is clicked', function () {
	it('should log out user', function () {
	    this.view.$('a#logout').click();

	    expect(Croffers.current_user).toBeUndefined();
	});

	it('should redirect to application root', function () {
	    expect(Croffers.router.navigate).toHaveBeenCalledWith(
		'/', { trigger: true });
	});
    });

    afterAll(function () {
	this.view.destroy();
    });
});
