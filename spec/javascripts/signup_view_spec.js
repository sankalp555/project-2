describe('Signup view', function () {
    var SUCCESSFUL_REGISTRATION_RESPONSE = {
	email: 'user@example.com',
	first_name: 'John',
	last_name: 'Smith'
    };

    var INVALID_REGISTRATION_RESPONSE = {
	error: 'short password'
    };

    beforeAll(function () {
	this.view = new Croffers.views.signup();
    });

    it('should create a form when initialized', function () {
	expect($('#signup-modal form').html()).not.toBeUndefined();
    });

    it('should remove itself when overlay is clicked', function () {
	$('.cell').click();
	expect($('#signup-popup').html()).toBeUndefined();
    });

    describe('when "Sign up" button is clicked with valid information filled in', function () {
	beforeAll(function () {
	    this.view = new Croffers.views.signup();

	    // simulate event dispatcher
	    Croffers.vent = {
		trigger: function (value) {}
	    };
	    spyOn(Croffers.vent, 'trigger');

	    // simulate application router
	    Croffers.router = {
		navigate: function (location, options) {}
	    };
	    spyOn(Croffers.router, 'navigate');

	    // simulate getting XHR response from server
	    spyOn($, 'ajax').and.callFake(function(options) {
		options.success(SUCCESSFUL_REGISTRATION_RESPONSE);
	    });
	});

	it('should save user information to database and create user model',
	   function () {
	       this.view.$('#email').val('user@example.com');
	       this.view.$('#password').val('12345678');
	       this.view.$('#first_name').val('John');
	       this.view.$('#last_name').val('Smith');

	       this.view.$('input[type=submit]').click();

	       expect(Croffers.current_user).toBeDefined();
	       expect(Croffers.current_user.get('email'))
		   .toEqual('user@example.com');
	   });

	it('should trigger session_change event', function () {
	    expect(Croffers.vent.trigger).toHaveBeenCalledWith('session_change');
	});

	it('should display profile subview', function () {
	    expect(Croffers.router.navigate).toHaveBeenCalledWith(
		'/profile/profile', {trigger: true});
	});
    });

    describe('when "Sign up" button is clicked with invalid information filled in', function () {
	it('should respond with error messages in corresponding form fields', function () {
	    pending('error handling, not urgent');
	});
    });
});
