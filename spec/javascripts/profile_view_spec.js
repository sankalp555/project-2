describe('Profile view', function () {
    beforeAll(function () {
	this.view = new Croffers.views.profile();
	this.subsections =
	    ['Listings', 'Inbox', 'Profile', 'Reservations', 'Dashboard'];

	Croffers.router = {
	    navigate: function (location, options) {}
	};
	spyOn(Croffers.router, 'navigate');
    });

    it('should render profile sidebar', function () {
	expect($('div#profile-sidebar').html()).not.toBeUndefined();
    });

    it('should display profile dashboard when initialized without argument',
       function () {
	   expect($('#profile-main').html()).toMatch(/dashboard/i);
       });

    it('should display another subsection when sidebar link is clicked',
       function() {
	   this.subsections.forEach(function (subsection) {
	       $('a:contains(' + subsection + ')').click();
	       expect(Croffers.router.navigate)
		   .toHaveBeenCalledWith('profile/' + subsection.toLowerCase());
	       expect($('#profile-main').html())
		   .toMatch(RegExp(subsection, 'i'));
	   });
       });

    it('should display subsection when initialized with argument', function () {
	var el;

	this.view.remove();
	expect($('#profile-main').html()).toBeUndefined();

	this.subsections.forEach(function (item) {
	    this.view = new Croffers.views.profile(item.toLowerCase());
	    expect(this.view.$('#profile-main').html()).toMatch(
		RegExp(item, 'i'));

	    this.view.remove();
	});
    });

    afterAll(function () {
	this.view.remove();
    });
});
