describe('User model', function () {
    beforeAll(function () {
	Backbone.sync = function (method, model, options) {};

	spyOn(Backbone, 'sync');
    });

    // not neccessary yet
    xit('should always have an email address associated with it', function () {
	var user = new Croffers.models.user();
	expect(user.isValid()).toBe(false);

	var valid_user = new Croffers.models.user({email: 'user@example.com'});
	expect(valid_user.isValid()).toBe(true);
    });
});
