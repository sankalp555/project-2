describe('Create hotel view', function () {
    beforeAll(function () {
	this.view = new Croffers.views.create_modify_hotel();
    });

    it('should generate a form when initialized', function () {
	expect(this.view.$('form')).not.toBeUndefined();
    });

    it('should automatically save changed form contents', function () {
	this.view.$('form #name').content('somename');
	expect(this.view.model.attributes).toContain({name: 'somename'});
    });

    it('should display thumbnails of chosen photographs', function () {
	// TODO
    });

    afterAll(function () {
	this.view.remove();
    });
});
