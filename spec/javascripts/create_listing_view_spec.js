describe('Create listing view', function () {
    beforeAll(function () {
	this.view = new Croffers.views.create_listing();
    });

    it('should contain \'Choose listing\' when initialized', function () {
	expect(this.view.$el.html()).toMatch(/Choose listing/);
    });

    it('should redirect to /app/create_listings/hotel when hotel icon is clicked',
       function () {
	   Croffers.router = {
	       navigate: function (destination, options) {}
	   };
	   spyOn(Croffers.router, 'navigate');

	   this.view.$('a:contains(Hotel)').click();
	   expect(Croffers.router.navigate).toHaveBeenCalledWith(
	       '/hotel/new', { trigger: true });
       });

    afterAll(function () {
	this.view.remove();
    });
});
