require 'rails_helper'

RSpec.describe 'User', type: :request do
  # before(:all) do
  #   @user = User.create!(email: 'toni.budrovic@gmail.com',
  #                        password: '12345678',
  #                        first_name: 'Toni',
  #                        last_name: 'Budrovic')
  # end

  it 'should update the resource' do
#    post :create, { data: { type: 'users', id: @user.id, attributes: { last_name: 'Budrovich' }}}
    post '/api/v1/users', { data: { type: 'users', attributes: { last_name: 'Budrovich' }}},
         {'HTTP_ACCEPT' => 'application/vnd.api+json', 'CONTENT_TYPE' => 'application/vnd.api+json'}

    puts response
    expect(response).to have_http_status(:ok)
  end
end
