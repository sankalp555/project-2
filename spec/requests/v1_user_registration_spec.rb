require 'rails_helper'

RSpec.describe 'User registration', type: :request do
  it 'should create user without password confirmation provided' do
    post '/api/v1/users/registration.json', {
           email: 'user@example.com',
           password: '12345678',
           first_name: 'John',
           last_name: 'Smith' }

    expect(response).to be_success
  end
end
