require "rails_helper"

RSpec.describe UserMailer, type: :mailer do
  describe 'welcome mail' do
    before :all do
      @email = UserMailer.welcome_email(UserSubscription.new(email:'a@b.com'))
    end

    it 'should contain subject "Welcome to Croffers"' do
      expect(@email.subject).to eq("Welcome to Croffers")
    end

    it 'should contain text "Thank you for signing up" in html and plain text' do
      expect(@email.html_part.body).to match(/Thank you for signing up/)
      expect(@email.text_part.body).to match(/Thank you for signing up/)
    end
  end
end
