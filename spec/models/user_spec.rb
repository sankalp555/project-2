require 'rails_helper'

RSpec.describe User, type: :model do
  # describe 'when created' do
  #   it 'should require user_type to be specified as either \'person\' or \'company\'' do
  #     @user = User.new email: 'user@example.com',
  #                      password: 'asjkdfjklsfd'
  #     expect(@user).not_to be_valid
  #     expect(@user.errors.messages).to include :user_type
  #   end
  # end

#  describe 'when user_type is \'person\'' do
    describe 'when created' do
      it 'should require email, password, first name and last name' do
        @user = User.new email: 'user@example.com',
                         password: 'kjjjjkajksdfj',
                         last_name: 'jkasdkjf'
        expect(@user).not_to be_valid
      end

      it 'should restrict first_name to <= 50 characters' do
        @user = User.new email: 'u@e.com',
                         password: '12345678',
                         first_name: 'a' * 51,
                         last_name: 'bbb'
        expect(@user).not_to be_valid
      end

      it 'should restrict last_name to <= 50 characters' do
        @user = User.new email: 'u@e.com',
                         password: '12345678',
                         first_name: 'bbb',
                         last_name: 'a' * 51
        expect(@user).not_to be_valid
      end

      it 'should be valid when all required fields are set' do
        @user = User.create email: 'user@example.com', password: 'example1',
                            first_name: 'Toni', last_name: 'Budrovic'
        expect(@user).to be_valid
        expect(@user.first_name).to eq(User.find_by(first_name: 'Toni').first_name)
      end
    end

    describe 'when updated' do
      it 'should allow additional fields to be set' do
        @user = User.create email: 'user2@example.com', password: 'example1',
                            first_name: 'Tnoi', last_name: 'Budroicv'
        @user.update oib: '1234567890987', birth_day: '13', birth_month: '06',
                     birth_year: '1990', telephone_number: '0959137151'
        @user.save!

        expect(@user.oib).to eq(User.find_by(oib: '1234567890987').oib)
      end
    end
#  end
end
