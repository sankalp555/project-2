require 'rails_helper'

RSpec.describe Hotel, type: :model do
  describe 'when created' do
    before(:all) do
      @hotel = Hotel.new
      @hotel.valid?
    end

    it 'should require user association to be set' do
      expect(@hotel.errors.messages).to include :user_id
    end

    it 'should have a list of rooms that belong to it' do
      expect(@hotel.rooms).to be_defined
    end

    it 'should have a name' do
      expect(@hotel.errors.messages).to include :name
    end

    it 'should have a description' do
      expect(@hotel.errors.messages).to include :description
    end

    it 'should have a check in and check out times set' do
      expect(@hotel.errors.messages).to include :check_in, :check_out
    end

    it 'should be valid when all of the above is provided' do
      @hotel.assign_attributes(
        { user: User.create({email: 'u@e.com', password: 'djkfsddfjk',
                             first_name: 'j', last_name: 's'}),
          name: 'Awesome hotel',
          description: 'This hotel has a very profound mission: passing an
 unit test',
          check_in: '13:00',
          check_out: '11:00'
        })

      expect(@hotel).to be_valid
    end

    it 'should restrict name to 50 characters' do
      @hotel.assign_attributes({ name: 'a' * 51 })
      expect(@hotel).not_to be_valid
      expect(@hotel.errors.messages).to include :name
    end

    it 'should restrict description to 500 words' do
      @hotel.assign_attributes({ name: 'a' * 49, description: 'a ' * 501 })
      expect(@hotel).not_to be_valid
      expect(@hotel.errors.messages).to include :description
    end

    # TODO: check formats of check_in and check_out
  end
end
