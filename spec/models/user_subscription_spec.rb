require 'rails_helper'

RSpec.describe UserSubscription, type: :model do
  describe 'when created' do
    it 'should require email address' do
      @user = UserSubscription.new
      expect(@user).not_to be_valid
    end

    it 'should permit is_business' do
      @user = UserSubscription.new({email: 'jjkjkj@sj.com'})
      expect(@user).to be_valid
      @user.is_business = true
      expect(@user).to be_valid
    end
  end
end
