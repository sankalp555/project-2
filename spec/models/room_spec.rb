require 'rails_helper'

RSpec.describe Room, type: :model do
  describe 'when created' do
    before(:all) do
      # TODO create a valid Room object
      @room = Room.new
      @room.valid?
    end

    it 'should have a number of people it accommodates set' do
      @room.update_attributes(:capacity, nil)
      expect(@room.errors.messages).to include :capacity
    end

    it 'should have a name' do
      expect(@room.errors.messages).to include :name
    end

    it 'should have a description' do
      expect(@room.errors.messages).to include :description
    end

    it 'should have an parent object set (Private accommodation or Hotel)' do
      expect(@room.errors.messages).to include :object_id
    end

    it 'should have an owner' do
      expect(@room.errors.messages).to include :user_id
    end
  end

  describe 'calendar' do
    it 'should be accessible from the Room object' do
      expect(@room.calendar_intervals).to be_defined
    end
  end
end
