require 'spec_helper'

describe 'app/index.html.erb', type: :view do
  describe 'when first loaded' do
    it 'should contain current_user data if user is logged in' do
      @user = User.create({email: 'sdjkf@fjk.com',
                           password: 'jdfkjsdfjsk',
                           first_name: 'Ivan',
                           last_name: 'Horvat'})
      sign_in @user
      render

      expect(rendered).to match /Croffers.current_user = new Croffers.models.user/
    end
  end
end
