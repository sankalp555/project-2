require 'spec_helper'

describe 'static/index.html.erb', type: :view do
  describe 'when loaded' do
    it 'should contain appropriate text' do
      render
      expect(rendered).to have_text 'Ever dreamed of the perfect vacation?'
    end
  end
end
