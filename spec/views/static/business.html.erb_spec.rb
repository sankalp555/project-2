# coding: utf-8
require 'spec_helper'

describe 'static/business.html.erb', type: :view do
  describe 'when loaded' do
    it 'should contain appropriate text' do
      render
      expect(rendered).to have_text 'Želite li svom gostu pružiti'
    end
  end
end
