require 'spec_helper'

RSpec.describe 'static/signed_up.html.erb', type: :view do
  describe 'when loaded' do
    it 'should contain Croffers logo' do
      render
      expect(rendered).to have_tag 'img'
    end

    it 'should contain appropirate text' do
      render
      expect(rendered).to have_text 'Thank you for signing up'
    end
  end
end
