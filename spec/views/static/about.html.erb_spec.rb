require 'spec_helper'

describe 'static/about.html.erb', type: :view do
  describe 'when loaded' do
    it 'should contain appropriate text' do
      user = UserSubscription.new
      assign :user, user

      render
      expect(rendered).to have_text 'We are Croffers, and we\'re here'
    end
  end
end
