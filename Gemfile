source 'https://rubygems.org'

ruby '2.2.4'

gem 'rails', '~> 4.2'
gem 'pg', '~> 0.18'

gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'
gem 'ejs' # embedded javascript
gem 'activeadmin'

gem 'devise', '~> 4.0'
gem 'paperclip', '~> 4.3'
gem 'mini_magick', '~> 4.8'
gem 'aws-sdk', '< 2.0'
gem 'pundit', '~> 1.1'
gem 'mangopay'
gem 'kaminari' # pagination
gem 'cancancan'
gem 'jquery-ui-rails'
gem 'rails-jquery-autocomplete'
gem "cocoon"
gem "font-awesome-rails"

# API related stuff

source 'http://insecure.rails-assets.org' do
  gem 'rails-assets-jquery'
  gem 'rails-assets-jquery-ujs'
  gem 'rails-assets-jquery-ui', '~> 1.11'
  gem 'rails-assets-jquery-ui-daterangepicker'
  gem 'rails-assets-knockoutjs'
  gem 'rails-assets-fullcalendar'
  gem 'rails-assets-moment'
  gem 'rails-assets-leaflet'
  gem 'rails-assets-leaflet.markercluster', '1.0.0.rc.1'
  gem 'rails-assets-slicknav'
end

# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0', group: :doc

group :development, :test do
  gem "letter_opener", :group => :development
  gem 'capistrano', '~> 3.5'
  gem 'capistrano-rails'
  gem 'capistrano-bundler'
  gem 'faker'
  gem 'byebug'
  gem 'web-console', '~> 2.0'
  gem 'figaro'
end

group :test do
  gem 'cucumber-rails', require: false
  gem 'rspec-rails'
  gem 'capybara'
  gem 'database_cleaner'
  gem 'factory_girl_rails'
end

group :production do
  gem 'unicorn'
  gem 'rails_12factor', group: :production
end

# For pdf generate
gem 'pdfkit'
gem 'wkhtmltopdf-binary'
