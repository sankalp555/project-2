class CreateRooms < ActiveRecord::Migration
  include RoomsHelper

  def change
    create_table :rooms do |t|
      t.references :lodging_provider, polymorphic: true, null: false

      t.string :name, limit: 50, null: false
      t.text   :description, limit: 500, null: false
      t.boolean :available_year_round, null: false

      room_amenities.each do |i|
        t.string i[:symbol], limit: 20
      end

      bathroom.each do |i|
        t.string i[:symbol], limit: 20
      end

      media_technology.each do |i|
        t.string i[:symbol], limit: 20
      end

      t.timestamps null: false
    end
  end
end
