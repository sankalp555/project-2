class RenameVehiclesToRentVehicles < ActiveRecord::Migration
  def change
    rename_table :vehicles, :rent_vehicles
  end
end
