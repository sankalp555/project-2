class CreateCharters < ActiveRecord::Migration
  def change
    create_table :charters do |t|
      t.string :name
      t.text :description
      t.string :yatch_type
      t.string :flag
      t.string :home_port
      
      t.string :make
      t.string :model
      t.string :engine_type
      t.integer :quantity
      t.integer :total_power
      
      t.integer :length
      t.integer :beam
      t.integer :draft
      t.integer :gross_tonnage
      
      t.integer :max_speed
      t.integer :cruising_speed
      t.integer :range
      t.integer :fuel_consumption
      t.integer :gasoil_tank
      
      t.string :builder
      t.integer :year_of_build
      t.integer :refit
      t.string :exterior_designer
      
      t.integer :guest_sleeping
      t.integer :guest_cruising

      t.boolean :air_conditioning
      t.boolean :barbecue

      t.boolean :deck_jacuzzi
      t.boolean :stabilizers

      t.boolean :television
      t.boolean :internet

      t.boolean :tender
      t.boolean :jetski
      t.boolean :stand_up_jetski
      t.boolean :water_ski
      t.boolean :paddle_board
      t.boolean :wake_board
      t.boolean :trampoline
      t.boolean :seabob
      t.boolean :scuba_diving
      t.boolean :inflatable_device

      t.timestamps null: false
    end
  end
end
