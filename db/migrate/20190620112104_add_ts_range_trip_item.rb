class AddTsRangeTripItem < ActiveRecord::Migration
  def change
  	add_column :trip_items, :start_date, :date
  	add_column :trip_items, :end_date, :date
  end
end
