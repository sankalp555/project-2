class ChangeOibInUsers < ActiveRecord::Migration
  def change
    change_column :users, :oib, :bigint
  end
end
