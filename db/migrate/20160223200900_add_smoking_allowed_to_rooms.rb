class AddSmokingAllowedToRooms < ActiveRecord::Migration
  def change
    add_column :rooms, :smoking_allowed, :string, limit: 20
  end
end
