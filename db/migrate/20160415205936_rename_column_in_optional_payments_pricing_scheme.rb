class RenameColumnInOptionalPaymentsPricingScheme < ActiveRecord::Migration
  def change
    rename_column :optional_payments_pricing_schemes, :type, :payment_type
  end
end
