class CreateListings < ActiveRecord::Migration
  def change
    create_table :listings do |t|
	t.references :listable, polymorphic: true, index: true
    end
  end
end
