class CreateLighthouses < ActiveRecord::Migration
  def change
    create_table :lighthouses do |t|
      t.string :name, { limit: 50, null: false }
      t.text :description, { limit: 500 }

      t.time :check_in
      t.time :check_out

      t.integer :max_adults, { limit: 5, default: 0 }
      t.integer :max_children, { limit: 5, default: 0 }
      t.integer :single_beds, { limit: 5, default: 0 }
      t.integer :double_beds, { limit: 5, default: 0 }

      t.timestamps
    end
  end
end
