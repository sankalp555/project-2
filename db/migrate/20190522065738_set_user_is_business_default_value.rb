class SetUserIsBusinessDefaultValue < ActiveRecord::Migration
  def change
		change_column :users, :is_business, :boolean, :default => false
  end
end
