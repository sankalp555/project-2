class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.references :sender
      t.references :receiver
      t.references :message_thread

      t.text :content, limit: 2000
      t.boolean :read, default: false
      t.string :type, limit: 20

      t.index :sender_id
      t.index :receiver_id
      t.index :message_thread_id

      t.timestamps
    end
  end
end
