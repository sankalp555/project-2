class CreateRentPricingScheme < ActiveRecord::Migration
  def change
    create_table :rent_pricing_schemes do |t|
      t.references :rent_vehicle

      t.string :name, limit: 50
      t.integer :base_rate, limit: 5
      t.integer :weekly_rate, limit: 5
      t.integer :monthly_rate, limit: 5
      t.integer :yearly_rate, limit: 5
    end
  end
end
