class CreateReservationPackage < ActiveRecord::Migration
  def change
    create_table :reservation_packages do |t|
      t.references :user
      t.date :date_from
      t.date :date_to
      t.timestamps
    end
  end
end
