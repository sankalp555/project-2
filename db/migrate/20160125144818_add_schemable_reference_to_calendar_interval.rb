class AddSchemableReferenceToCalendarInterval < ActiveRecord::Migration
  def change
    add_reference :calendar_intervals, :schemable, polymorphic: true, index: true
  end
end
