class ChangeRooms < ActiveRecord::Migration
  def change
    add_column :rooms, :max_adults, :integer, limit: 5, default: 0
    add_column :rooms, :max_children, :integer, limit: 5, default: 0
    add_column :rooms, :size, :integer, limit: 5
    add_column :rooms, :single_beds, :integer, limit: 5, default: 0
    add_column :rooms, :double_beds, :integer, limit: 5, default: 0
    add_column :rooms, :queen_beds, :integer, limit: 5, default: 0
    add_column :rooms, :king_beds, :integer, limit: 5, default: 0
    add_column :rooms, :bunk_beds, :integer, limit: 5, default: 0
    add_column :rooms, :sofa_beds, :integer, limit: 5, default: 0
    add_column :rooms, :futons, :integer, limit: 5, default: 0
  end
end
