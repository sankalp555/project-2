class AddPriceToReservations < ActiveRecord::Migration
  def change
    add_column :reservations, :price, :decimal,
               { precision: 7, scale: 2, null: false, default: 0 }
  end
end
