class CreateUserSubscriptions < ActiveRecord::Migration
  def change
    create_table :user_subscriptions do |t|
      t.string  :email
      t.boolean :is_business

      t.timestamps null: false
    end
  end
end
