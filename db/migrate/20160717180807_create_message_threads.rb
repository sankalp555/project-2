class CreateMessageThreads < ActiveRecord::Migration
  def change
    create_table :message_threads do |t|
      t.references :sender
      t.references :receiver
      t.references :reservation

      t.index :sender_id
      t.index :receiver_id
      t.index :reservation_id

      t.timestamps
    end
  end
end
