class AddColumnsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :company_name, :string, { limit: 100 }
    add_column :users, :vat_id, :string, { limit: 50 }
    add_column :users, :is_business, :boolean
  end
end
