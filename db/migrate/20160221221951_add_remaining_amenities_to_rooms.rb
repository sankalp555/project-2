class AddRemainingAmenitiesToRooms < ActiveRecord::Migration
  include RoomsHelper
  
  def change
    food_drink.each do |i|
      add_column :rooms, i[:symbol], :string, limit: 20
    end

    services_extras.each do |i|
      add_column :rooms, i[:symbol], :string, limit: 20
    end

    outdoor_view.each do |i|
      add_column :rooms, i[:symbol], :string, limit: 20
    end
  end
end
