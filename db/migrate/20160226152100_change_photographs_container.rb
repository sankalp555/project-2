class ChangePhotographsContainer < ActiveRecord::Migration
  def change
    remove_reference :photographs, :photographable, polymorphic: true, index: true
    add_reference :photographs, :galleries
  end
end
