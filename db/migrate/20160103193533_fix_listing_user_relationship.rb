class FixListingUserRelationship < ActiveRecord::Migration
  def change
    add_reference :listings, :user, index: true
    remove_reference :hotels, :user, index: true
  end
end
