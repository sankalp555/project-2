class CreatePricingSchemeAccommodations < ActiveRecord::Migration
  def change
    create_table :pricing_scheme_accommodations do |t|
      t.references :lodging_provider, polymorphic: true, null: false

      t.string :name, limit: 50, null: false

      t.decimal :base_rate, precision: 7, scale: 2, null: false
      t.decimal :weekly_rate, precision: 7, scale: 2
      t.decimal :extra_guests_amount, precision: 7, scale: 2

      t.integer :min_length

      t.integer :long_stay_discount_threshold
      t.decimal :long_stay_discount_percentage, precision: 2, scale: 2

      t.timestamps null: false
    end
  end
end
