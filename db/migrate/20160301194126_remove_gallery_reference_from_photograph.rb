class RemoveGalleryReferenceFromPhotograph < ActiveRecord::Migration
  def change
    remove_reference :photographs, :gallery
  end
end
