class CreateGalleries < ActiveRecord::Migration
  def change
    create_table :galleries do |t|
      t.references :presentable, polymorphic: true, index: true
    end
  end
end
