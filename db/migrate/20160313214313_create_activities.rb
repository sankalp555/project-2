class CreateActivities < ActiveRecord::Migration
  def change
    create_table :activities do |t|
      t.string :name, { limit: 50 }
      t.text :description, { limit: 500 }
    end
  end
end
