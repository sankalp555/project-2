class ChangeStatusTypeInReservations < ActiveRecord::Migration
  def change
    remove_column :reservations, :status
    add_column :reservations, :status, :integer, null: false, default: 0
  end
end
