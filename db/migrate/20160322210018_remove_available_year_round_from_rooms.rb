class RemoveAvailableYearRoundFromRooms < ActiveRecord::Migration
  def change
    remove_column :rooms, :available_year_round
  end
end
