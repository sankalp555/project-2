class RenamePricingSchemeAccommodationsToRoomPricingSchemes < ActiveRecord::Migration
  def change
    rename_table :pricing_scheme_accommodations, :room_pricing_schemes
  end
end
