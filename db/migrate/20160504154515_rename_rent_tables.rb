class RenameRentTables < ActiveRecord::Migration
  def change
    rename_table :rent_vehicles, :vehicles
  end
end
