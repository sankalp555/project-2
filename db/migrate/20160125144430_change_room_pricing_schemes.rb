class ChangeRoomPricingSchemes < ActiveRecord::Migration
  def change
    remove_reference :room_pricing_schemes, :lodging_provider
    add_reference :room_pricing_schemes, :room, index: true
  end
end
