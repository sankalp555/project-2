class CreateTripItems < ActiveRecord::Migration
  def change
    create_table :trip_items do |t|
      t.references :trip_destination, index: true
      t.string :item_id
      t.string :item_type
      t.decimal :price
      t.timestamps null: false
    end
  end
end
