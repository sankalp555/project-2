class AddColumnsToCalendarIntervals < ActiveRecord::Migration
  def change
    add_column :calendar_intervals, :unavailable, :boolean
  end
end
