class CreateVehicles < ActiveRecord::Migration
  def change
    create_table :vehicles do |t|
      t.references :rent

      t.string :type
      t.string :name, { limit: 50 }
      t.text :description, { limit: 500 }
      t.integer :amount, { limit: 8 }
      t.integer :capacity, { limit: 8 }
      t.integer :engine_cc, { limit: 8 }
      t.integer :engine_hp, { limit: 8 }

      t.timestamps
    end
  end
end
