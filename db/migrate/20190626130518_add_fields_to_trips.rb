class AddFieldsToTrips < ActiveRecord::Migration
  def change
    add_column :trips, :start_date, :date
    add_column :trips, :end_date, :date
    add_column :trips, :no_of_guest, :integer
    rename_column :trip_destinations, :destination, :city_id
  end
end
