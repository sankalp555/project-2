class CreateInvoiceLines < ActiveRecord::Migration
  def change
    create_table :invoice_lines do |t|
      t.references :invoice, index: true
      t.references :reservation, index: true
      t.string :name
      t.decimal :amount
      t.timestamps null: false
    end
  end
end
