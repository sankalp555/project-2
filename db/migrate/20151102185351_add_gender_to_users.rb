class AddGenderToUsers < ActiveRecord::Migration
  def change
    add_column :users, :gender, :string, { limit: 50 }
  end
end
