class AddReferenceToPhotographs < ActiveRecord::Migration
  def change
    add_reference :photographs, :presentable, polymorphic: true, index: true
    rename_column :photographs, :serial_no, :sequence_number
  end
end
