class CreateInvoices < ActiveRecord::Migration
  def change
    create_table :invoices do |t|
      t.references :user, index: true
      t.references :payment, index: true
      t.timestamps null: false
    end
  end
end
