class AddExpiresToTrips < ActiveRecord::Migration
  def change
    add_column :trips, :expires, :datetime
  end
end
