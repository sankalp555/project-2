class CreatePrivateAccommodation < ActiveRecord::Migration
  # private accommodation and hotels share amenities
  include HotelsHelper
  
  def change
    create_table :private_accommodations do |t|
      t.references :user

      t.string :name, { limit: 50 }
      t.text :description, { limit: 500 }

      t.time :check_in
      t.time :check_out

      hotel_facilities_general.each do |f|
        t.string f[:symbol], { limit: 20 }
      end

      hotel_facilities_activities.each do |f|
        t.string f[:symbol], { limit: 20 }
      end

      hotel_facilities_services.each do |f|
        t.string f[:symbol], { limit: 20 }
      end

      t.timestamps
    end
  end
end
