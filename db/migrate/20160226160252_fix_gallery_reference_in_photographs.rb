class FixGalleryReferenceInPhotographs < ActiveRecord::Migration
  def change
    remove_reference :photographs, :galleries
    add_reference :photographs, :gallery
  end
end
