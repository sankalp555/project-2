class ModifyBirthDateInUsers < ActiveRecord::Migration
  def change
    change_table :users do |t|
      t.remove :birth_day, :birth_month, :birth_year
      t.date :birth_date
    end
  end
end
