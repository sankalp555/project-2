class AddStartDatetimeEndDatetimeToEvents < ActiveRecord::Migration
  def change
    rename_column :events, :datetime, :start_datetime
    add_column :events, :end_datetime, :datetime
  end
end
