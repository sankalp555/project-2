class RenameRentVehicleIdToVehicleIdInRentPricingSchemes < ActiveRecord::Migration
  def change
    rename_column :rent_pricing_schemes, :rent_vehicle_id, :vehicle_id
  end
end
