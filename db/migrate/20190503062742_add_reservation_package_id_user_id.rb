class AddReservationPackageIdUserId < ActiveRecord::Migration
  def change
  	add_reference :payments, :reservation_package, index: true
  	add_reference :payments, :user, index: true
  end
end
