class CreateCalendarIntervals < ActiveRecord::Migration
  def change
    create_table :calendar_intervals do |t|
      t.references :scheduable, polymorphic: true, null: false

      t.daterange :date_span, null: false

      t.timestamps null: false
    end
  end
end
