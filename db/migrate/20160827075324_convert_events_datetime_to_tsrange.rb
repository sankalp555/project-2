class ConvertEventsDatetimeToTsrange < ActiveRecord::Migration
  def change
    add_column :events, :tsrange, :tsrange

    reversible do |event|
      event.up do
        execute <<-SQL
          UPDATE events SET tsrange = tsrange(start_datetime::timestamp, end_datetime::timestamp, '[]');
        SQL
      end

      event.down do
        execute <<-SQL
          UPDATE events SET start_datetime = lower(tsrange), end_datetime= upper(tsrange);
        SQL
      end
    end

    remove_column :events, :start_datetime, :datetime
    remove_column :events, :end_datetime, :datetime
  end
end
