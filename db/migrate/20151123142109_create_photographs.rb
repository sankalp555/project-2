class CreatePhotographs < ActiveRecord::Migration
  def change
    create_table :photographs do |t|
      t.references :photographable, polymorphic: true, index: true

      t.string  :filename
      t.integer :serial_no
      t.text    :description, limit: 200

      t.timestamps null: false
    end
  end
end
