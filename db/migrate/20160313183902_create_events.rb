class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :name, { limit: 50 }
      t.text :description, { limit: 500 }
      t.datetime :datetime
      t.integer :capacity, { limit: 8 }
      t.integer :base_rate, { limit: 8 }
    end
  end
end
