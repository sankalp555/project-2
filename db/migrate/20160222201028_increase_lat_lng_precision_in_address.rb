class IncreaseLatLngPrecisionInAddress < ActiveRecord::Migration
  def change
    change_column :addresses, :longitude, :decimal, precision: 23, scale: 20
    change_column :addresses, :latitude, :decimal, precision: 23, scale: 20
  end
end
