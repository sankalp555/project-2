class AddTelephoneNumberToUsers < ActiveRecord::Migration
  def change
    add_column :users, :telephone_number, :string, { limit: 50 }
  end
end
