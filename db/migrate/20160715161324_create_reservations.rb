class CreateReservations < ActiveRecord::Migration
  def change
    create_table :reservations do |t|
      t.references :reservable, polymorphic: true
      t.references :reservation_package
      t.datetime :date_from
      t.datetime :date_to
      t.integer :number_of_people
      t.string :status, limit: 50
    end
  end
end
