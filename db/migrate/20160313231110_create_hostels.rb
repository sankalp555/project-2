class CreateHostels < ActiveRecord::Migration
  include HostelsHelper

  def change
    create_table :hostels do |t|
      t.string :name, { limit: 50 }
      t.text :description, { limit: 500 }

      t.time :check_in
      t.time :check_out

      hostel_facilities.each do |f|
        t.string f[:symbol], { limit: 20 }
      end

      t.timestamps
    end
  end
end
