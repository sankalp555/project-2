class RenameBusinessToBusinessable < ActiveRecord::Migration
  def change
  	rename_column :businesses, :business_id, :businessable_id
  	rename_column :businesses, :business_type, :businessable_type
  end
end
