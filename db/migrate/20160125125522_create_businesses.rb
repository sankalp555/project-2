class CreateBusinesses < ActiveRecord::Migration
  def change
    create_table :businesses do |t|
      t.references :business, polymorphic: true, null: false

      t.timestamps null:false
    end
  end
end
