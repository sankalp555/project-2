class AddPaperclipFieldToPhotographs < ActiveRecord::Migration
  def change
    remove_column :photographs, :filename
    add_attachment :photographs, :photograph
  end
end
