class RemoveLodgingProviderTypeFromRoomPricingScheme < ActiveRecord::Migration
  def change
    remove_column :room_pricing_schemes, :lodging_provider_type
  end
end
