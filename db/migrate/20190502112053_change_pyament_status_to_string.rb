class ChangePyamentStatusToString < ActiveRecord::Migration
  def change
  	remove_column :payments, :payment_status, :boolean
  	add_column :payments, :payment_status, :string
  end
end
