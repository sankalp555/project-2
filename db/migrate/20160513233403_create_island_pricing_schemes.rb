class CreateIslandPricingSchemes < ActiveRecord::Migration
  def change
    create_table :island_pricing_schemes do |t|
      t.belongs_to :island

      t.string :name, { limit: 50 }
      t.integer :daily_rate, { limit: 5 }

      t.timestamps
    end
  end
end
