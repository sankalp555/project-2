class AddFacilitiesToHotels < ActiveRecord::Migration
  include HotelsHelper

  def change
    hotel_facilities_general.each do |f|
      add_column :hotels, f[:symbol], :string, limit: 20
    end

    hotel_facilities_activities.each do |f|
      add_column :hotels, f[:symbol], :string, limit: 20
    end

    hotel_facilities_services.each do |f|
      add_column :hotels, f[:symbol], :string, limit: 20
    end
  end
end
