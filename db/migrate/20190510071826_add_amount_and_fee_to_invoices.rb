class AddAmountAndFeeToInvoices < ActiveRecord::Migration
  def change
  	add_column :invoices, :amount, :decimal
  	add_column :invoices, :fee, :decimal
  end
end
