class CreateAddresses < ActiveRecord::Migration
  def change
    create_table :addresses do |t|
      t.references :addressable, polymorphic: true

      t.string :street_address, limit: 100, null: false
      t.string :postal_code, limit: 30, null: false
      t.string :address_locality, limit: 50, null: false

      t.decimal :longitude, precision: 10, scale: 7
      t.decimal :latitude, precision: 10, scale: 7
    end
  end
end
