class AddressLocalityToCityId < ActiveRecord::Migration
  def change
    remove_column :addresses, :address_locality
    add_reference :addresses, :city, index: true
  end
end
