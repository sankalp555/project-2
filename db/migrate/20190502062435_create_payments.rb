class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.string :transaction_id
      t.decimal :amount
      t.decimal :fee
      t.string :currency
      t.boolean :payment_status
      t.string :payment_type
      t.string :credited_wallet_id
      t.string :credited_user_id
      t.string :payment_tag
      t.timestamps null: false
    end
  end
end
