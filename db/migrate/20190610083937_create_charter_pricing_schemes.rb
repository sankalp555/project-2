class CreateCharterPricingSchemes < ActiveRecord::Migration
  def change
    create_table :charter_pricing_schemes do |t|
      t.references :charter

      t.string :name, { limit: 50 }
      t.integer :base_rate, { limit: 8 }
      
      t.timestamps null: false
    end
  end
end
