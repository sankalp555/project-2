class CreateOptionalPaymentsPricingScheme < ActiveRecord::Migration
  def change
    create_table :optional_payments_pricing_schemes do |t|
      t.references :extendable_pricing_scheme, polymorphic: true, null: false

      t.string :description, limit: 100, null: false
      t.decimal :base_rate, precision: 7, scale: 2, null: false
      t.string :type, limit: 30, null: false

      t.timestamps
    end
  end
end
