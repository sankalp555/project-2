class CreateHotels < ActiveRecord::Migration
  def change
    create_table :hotels do |t|
      t.references :user

      t.string :name, { limit: 50 }
      t.text :description, { limit: 500 }

      t.time :check_in
      t.time :check_out

      t.timestamps
    end
  end
end
