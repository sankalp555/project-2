class CreateTripDestinations < ActiveRecord::Migration
  def change
    create_table :trip_destinations do |t|
      t.references :trip, index: true
      t.string :destination
      t.date :start_date
      t.date :end_date
      t.integer :no_of_guest
      t.timestamps null: false
    end
  end
end
