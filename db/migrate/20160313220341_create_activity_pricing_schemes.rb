class CreateActivityPricingSchemes < ActiveRecord::Migration
  def change
    create_table :activity_pricing_schemes do |t|
      t.references :activity

      t.string :name, { limit: 50 }
      t.integer :base_rate, { limit: 8 }
    end
  end
end
