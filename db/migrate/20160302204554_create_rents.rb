class CreateRents < ActiveRecord::Migration
  def change
    create_table :rents do |t|
      t.string :name, { limit: 50 }
      t.text :description, { limit: 500 }

      t.timestamps
    end
  end
end
