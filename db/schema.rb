# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20200115074937) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_admin_comments", force: :cascade do |t|
    t.string   "namespace"
    t.text     "body"
    t.integer  "resource_id"
    t.string   "resource_type"
    t.integer  "author_id"
    t.string   "author_type"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "activities", force: :cascade do |t|
    t.string "name",        limit: 50
    t.text   "description"
  end

  create_table "activity_pricing_schemes", force: :cascade do |t|
    t.integer "activity_id"
    t.string  "name",        limit: 50
    t.integer "base_rate",   limit: 8
  end

  create_table "addresses", force: :cascade do |t|
    t.integer "addressable_id"
    t.string  "addressable_type"
    t.string  "street_address",   limit: 100,                           null: false
    t.string  "postal_code",      limit: 30,                            null: false
    t.decimal "longitude",                    precision: 23, scale: 20
    t.decimal "latitude",                     precision: 23, scale: 20
    t.integer "city_id"
  end

  add_index "addresses", ["city_id"], name: "index_addresses_on_city_id", using: :btree

  create_table "admin_users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree

  create_table "businesses", force: :cascade do |t|
    t.integer  "businessable_id",   null: false
    t.string   "businessable_type", null: false
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.integer  "user_id"
  end

  add_index "businesses", ["user_id"], name: "index_businesses_on_user_id", using: :btree

  create_table "calendar_intervals", force: :cascade do |t|
    t.integer   "scheduable_id",   null: false
    t.string    "scheduable_type", null: false
    t.daterange "date_span",       null: false
    t.datetime  "created_at",      null: false
    t.datetime  "updated_at",      null: false
    t.integer   "schemable_id"
    t.string    "schemable_type"
    t.boolean   "unavailable"
  end

  add_index "calendar_intervals", ["schemable_type", "schemable_id"], name: "index_calendar_intervals_on_schemable_type_and_schemable_id", using: :btree

  create_table "charter_pricing_schemes", force: :cascade do |t|
    t.integer  "charter_id"
    t.string   "name",       limit: 50
    t.integer  "base_rate",  limit: 8
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  create_table "charters", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.string   "yatch_type"
    t.string   "flag"
    t.string   "home_port"
    t.string   "make"
    t.string   "model"
    t.string   "engine_type"
    t.integer  "quantity"
    t.integer  "total_power"
    t.integer  "length"
    t.integer  "beam"
    t.integer  "draft"
    t.integer  "gross_tonnage"
    t.integer  "max_speed"
    t.integer  "cruising_speed"
    t.integer  "range"
    t.integer  "fuel_consumption"
    t.integer  "gasoil_tank"
    t.string   "builder"
    t.integer  "year_of_build"
    t.integer  "refit"
    t.string   "exterior_designer"
    t.integer  "guest_sleeping"
    t.integer  "guest_cruising"
    t.boolean  "air_conditioning"
    t.boolean  "barbecue"
    t.boolean  "deck_jacuzzi"
    t.boolean  "stabilizers"
    t.boolean  "television"
    t.boolean  "internet"
    t.boolean  "tender"
    t.boolean  "jetski"
    t.boolean  "stand_up_jetski"
    t.boolean  "water_ski"
    t.boolean  "paddle_board"
    t.boolean  "wake_board"
    t.boolean  "trampoline"
    t.boolean  "seabob"
    t.boolean  "scuba_diving"
    t.boolean  "inflatable_device"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  create_table "cities", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "events", force: :cascade do |t|
    t.string  "name",        limit: 50
    t.text    "description"
    t.integer "capacity",    limit: 8
    t.integer "base_rate",   limit: 8
    t.tsrange "tsrange"
  end

  create_table "hostels", force: :cascade do |t|
    t.string   "name",                limit: 50
    t.text     "description"
    t.time     "check_in"
    t.time     "check_out"
    t.string   "non_stop_reception",  limit: 20
    t.string   "non_stop_security",   limit: 20
    t.string   "adoptors",            limit: 20
    t.string   "air_conditioning",    limit: 20
    t.string   "airport_transfers",   limit: 20
    t.string   "atm",                 limit: 20
    t.string   "bar",                 limit: 20
    t.string   "bbq",                 limit: 20
    t.string   "bicycle_hire",        limit: 20
    t.string   "bicycle_parking",     limit: 20
    t.string   "board_games",         limit: 20
    t.string   "book_exchange",       limit: 20
    t.string   "breakfast",           limit: 20
    t.string   "cable",               limit: 20
    t.string   "cafe",                limit: 20
    t.string   "ceiling_fan",         limit: 20
    t.string   "childrens_play",      limit: 20
    t.string   "common_room",         limit: 20
    t.string   "cooker",              limit: 20
    t.string   "cots",                limit: 20
    t.string   "exchange",            limit: 20
    t.string   "direct_dial_tel",     limit: 20
    t.string   "dishwasher",          limit: 20
    t.string   "dryer",               limit: 20
    t.string   "dvds",                limit: 20
    t.string   "elevator",            limit: 20
    t.string   "express",             limit: 20
    t.string   "fax",                 limit: 20
    t.string   "city_maps",           limit: 20
    t.string   "city_tour",           limit: 20
    t.string   "internet",            limit: 20
    t.string   "parking",             limit: 20
    t.string   "fridge",              limit: 20
    t.string   "fusball",             limit: 20
    t.string   "games",               limit: 20
    t.string   "gym",                 limit: 20
    t.string   "hairdryers",          limit: 20
    t.string   "hairdryers_hire",     limit: 20
    t.string   "hot_showers",         limit: 20
    t.string   "hot_tub",             limit: 20
    t.string   "housekeeping",        limit: 20
    t.string   "pool_indoor",         limit: 20
    t.string   "internet_access",     limit: 20
    t.string   "internet_cafe",       limit: 20
    t.string   "iron_board",          limit: 20
    t.string   "jobs_board",          limit: 20
    t.string   "key_card",            limit: 20
    t.string   "kitchen",             limit: 20
    t.string   "late_checkout",       limit: 20
    t.string   "laundry_facilities",  limit: 20
    t.string   "linen_included",      limit: 20
    t.string   "linen_not_included",  limit: 20
    t.string   "lockers",             limit: 20
    t.string   "luggage",             limit: 20
    t.string   "meals",               limit: 20
    t.string   "meeting",             limit: 20
    t.string   "microwave",           limit: 20
    t.string   "supermarket",         limit: 20
    t.string   "nightclub",           limit: 20
    t.string   "pool_outdoor",        limit: 20
    t.string   "terrace",             limit: 20
    t.string   "playstation",         limit: 20
    t.string   "pool",                limit: 20
    t.string   "postal",              limit: 20
    t.string   "reading_light",       limit: 20
    t.string   "reception",           limit: 20
    t.string   "restaurant",          limit: 20
    t.string   "safe",                limit: 20
    t.string   "sauna",               limit: 20
    t.string   "shuttle",             limit: 20
    t.string   "steam_room",          limit: 20
    t.string   "swimming_pool",       limit: 20
    t.string   "tea_coffee",          limit: 20
    t.string   "travel_desk",         limit: 20
    t.string   "towels_hire",         limit: 20
    t.string   "towels_included",     limit: 20
    t.string   "towels_not_included", limit: 20
    t.string   "utensils",            limit: 20
    t.string   "vending_machines",    limit: 20
    t.string   "wake_up_calls",       limit: 20
    t.string   "washing_machine",     limit: 20
    t.string   "wheelchair",          limit: 20
    t.string   "wifi",                limit: 20
    t.string   "wii",                 limit: 20
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "hotels", force: :cascade do |t|
    t.string   "name",                  limit: 50
    t.text     "description"
    t.time     "check_in"
    t.time     "check_out"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "front_desk",            limit: 20
    t.string   "adult_only",            limit: 20
    t.string   "air_conditioning",      limit: 20
    t.string   "allergy_free",          limit: 20
    t.string   "non_smoking_all",       limit: 20
    t.string   "bar",                   limit: 20
    t.string   "breakfast_buffet",      limit: 20
    t.string   "chapel",                limit: 20
    t.string   "smoking_area",          limit: 20
    t.string   "design_hotel",          limit: 20
    t.string   "elevator",              limit: 20
    t.string   "express_check",         limit: 20
    t.string   "family_rooms",          limit: 20
    t.string   "garden",                limit: 20
    t.string   "gay_friendly",          limit: 20
    t.string   "heating",               limit: 20
    t.string   "luggage_storage",       limit: 20
    t.string   "mini_market",           limit: 20
    t.string   "newspapers",            limit: 20
    t.string   "non_smoking_rooms",     limit: 20
    t.string   "private_beach",         limit: 20
    t.string   "restaurant",            limit: 20
    t.string   "restaurant_carte",      limit: 20
    t.string   "restaurant_buffet",     limit: 20
    t.string   "disabled",              limit: 20
    t.string   "safe",                  limit: 20
    t.string   "kitchen",               limit: 20
    t.string   "shops",                 limit: 20
    t.string   "ski_storage",           limit: 20
    t.string   "snack_bar",             limit: 20
    t.string   "soundproof",            limit: 20
    t.string   "sun_terrace",           limit: 20
    t.string   "terrace",               limit: 20
    t.string   "valet_parking",         limit: 20
    t.string   "aqua",                  limit: 20
    t.string   "bbq",                   limit: 20
    t.string   "beachfront",            limit: 20
    t.string   "billiards",             limit: 20
    t.string   "bowling",               limit: 20
    t.string   "canoeing",              limit: 20
    t.string   "casino",                limit: 20
    t.string   "playground",            limit: 20
    t.string   "cycling",               limit: 20
    t.string   "darts",                 limit: 20
    t.string   "diving",                limit: 20
    t.string   "evening_entertainment", limit: 20
    t.string   "fishing",               limit: 20
    t.string   "fitness",               limit: 20
    t.string   "games",                 limit: 20
    t.string   "golf",                  limit: 20
    t.string   "hiking",                limit: 20
    t.string   "riding",                limit: 20
    t.string   "bath",                  limit: 20
    t.string   "tub",                   limit: 20
    t.string   "pool_all",              limit: 20
    t.string   "pool_season",           limit: 20
    t.string   "pool_swimming",         limit: 20
    t.string   "karaoke",               limit: 20
    t.string   "kids_club",             limit: 20
    t.string   "library",               limit: 20
    t.string   "massage",               limit: 20
    t.string   "minigolf",              limit: 20
    t.string   "out_pool_all",          limit: 20
    t.string   "out_pool_season",       limit: 20
    t.string   "sauna",                 limit: 20
    t.string   "skiing",                limit: 20
    t.string   "ski_school",            limit: 20
    t.string   "snorkeling",            limit: 20
    t.string   "solarium",              limit: 20
    t.string   "spa",                   limit: 20
    t.string   "squash",                limit: 20
    t.string   "table_tennis",          limit: 20
    t.string   "tennis",                limit: 20
    t.string   "steam",                 limit: 20
    t.string   "windsurfing",           limit: 20
    t.string   "airport_shuttle",       limit: 20
    t.string   "atm",                   limit: 20
    t.string   "babysitting",           limit: 20
    t.string   "barber",                limit: 20
    t.string   "bicycle",               limit: 20
    t.string   "room_breakfast",        limit: 20
    t.string   "bridal_suite",          limit: 20
    t.string   "business_centre",       limit: 20
    t.string   "car_rental",            limit: 20
    t.string   "concierge",             limit: 20
    t.string   "exchange",              limit: 20
    t.string   "maid",                  limit: 20
    t.string   "dry_cleaning",          limit: 20
    t.string   "entertainment_staff",   limit: 20
    t.string   "fax",                   limit: 20
    t.string   "groceries",             limit: 20
    t.string   "ironing",               limit: 20
    t.string   "laundry",               limit: 20
    t.string   "lockers",               limit: 20
    t.string   "meeting",               limit: 20
    t.string   "nightclub",             limit: 20
    t.string   "lunches",               limit: 20
    t.string   "private_check",         limit: 20
    t.string   "room_service",          limit: 20
    t.string   "lounge",                limit: 20
    t.string   "shoe_shine",            limit: 20
    t.string   "shuttle",               limit: 20
    t.string   "ski_hire",              limit: 20
    t.string   "ski_pass",              limit: 20
    t.string   "ski_to_door",           limit: 20
    t.string   "souvenirs",             limit: 20
    t.string   "special_diet",          limit: 20
    t.string   "tickets",               limit: 20
    t.string   "tour",                  limit: 20
    t.string   "trouser_press",         limit: 20
    t.string   "vending_drinks",        limit: 20
    t.string   "vending_snacks",        limit: 20
    t.string   "vip_room",              limit: 20
    t.string   "water_sports",          limit: 20
  end

  create_table "invoice_lines", force: :cascade do |t|
    t.integer  "invoice_id"
    t.integer  "reservation_id"
    t.string   "name"
    t.decimal  "amount"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "invoice_lines", ["invoice_id"], name: "index_invoice_lines_on_invoice_id", using: :btree
  add_index "invoice_lines", ["reservation_id"], name: "index_invoice_lines_on_reservation_id", using: :btree

  create_table "invoices", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "payment_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.decimal  "amount"
    t.decimal  "fee"
  end

  add_index "invoices", ["payment_id"], name: "index_invoices_on_payment_id", using: :btree
  add_index "invoices", ["user_id"], name: "index_invoices_on_user_id", using: :btree

  create_table "island_pricing_schemes", force: :cascade do |t|
    t.integer  "island_id"
    t.string   "name",       limit: 50
    t.integer  "daily_rate", limit: 8
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "islands", force: :cascade do |t|
    t.string   "name",         limit: 50,             null: false
    t.text     "description"
    t.time     "check_in"
    t.time     "check_out"
    t.integer  "max_adults",   limit: 8,  default: 0
    t.integer  "max_children", limit: 8,  default: 0
    t.integer  "size",         limit: 8,  default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "lighthouse_pricing_schemes", force: :cascade do |t|
    t.integer  "lighthouse_id"
    t.string   "name",          limit: 50
    t.integer  "daily_rate",    limit: 8
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "lighthouses", force: :cascade do |t|
    t.string   "name",         limit: 50,             null: false
    t.text     "description"
    t.time     "check_in"
    t.time     "check_out"
    t.integer  "max_adults",   limit: 8,  default: 0
    t.integer  "max_children", limit: 8,  default: 0
    t.integer  "single_beds",  limit: 8,  default: 0
    t.integer  "double_beds",  limit: 8,  default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "listings", force: :cascade do |t|
    t.integer "listable_id"
    t.string  "listable_type"
    t.integer "user_id"
  end

  add_index "listings", ["listable_type", "listable_id"], name: "index_listings_on_listable_type_and_listable_id", using: :btree
  add_index "listings", ["user_id"], name: "index_listings_on_user_id", using: :btree

  create_table "message_threads", force: :cascade do |t|
    t.integer  "sender_id"
    t.integer  "receiver_id"
    t.integer  "reservation_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "message_threads", ["receiver_id"], name: "index_message_threads_on_receiver_id", using: :btree
  add_index "message_threads", ["reservation_id"], name: "index_message_threads_on_reservation_id", using: :btree
  add_index "message_threads", ["sender_id"], name: "index_message_threads_on_sender_id", using: :btree

  create_table "messages", force: :cascade do |t|
    t.integer  "sender_id"
    t.integer  "receiver_id"
    t.integer  "message_thread_id"
    t.text     "content"
    t.boolean  "read",                         default: false
    t.string   "message_type",      limit: 20
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "messages", ["message_thread_id"], name: "index_messages_on_message_thread_id", using: :btree
  add_index "messages", ["receiver_id"], name: "index_messages_on_receiver_id", using: :btree
  add_index "messages", ["sender_id"], name: "index_messages_on_sender_id", using: :btree

  create_table "optional_payments_pricing_schemes", force: :cascade do |t|
    t.integer  "extendable_pricing_scheme_id",                                       null: false
    t.string   "extendable_pricing_scheme_type",                                     null: false
    t.string   "description",                    limit: 100,                         null: false
    t.decimal  "base_rate",                                  precision: 7, scale: 2, null: false
    t.string   "payment_type",                   limit: 30,                          null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "payments", force: :cascade do |t|
    t.string   "transaction_id"
    t.decimal  "amount"
    t.decimal  "fee"
    t.string   "currency"
    t.string   "payment_type"
    t.string   "credited_wallet_id"
    t.string   "credited_user_id"
    t.string   "payment_tag"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.string   "payment_status"
    t.integer  "reservation_package_id"
    t.integer  "user_id"
    t.string   "event_type"
  end

  add_index "payments", ["reservation_package_id"], name: "index_payments_on_reservation_package_id", using: :btree
  add_index "payments", ["user_id"], name: "index_payments_on_user_id", using: :btree

  create_table "photographs", force: :cascade do |t|
    t.integer  "sequence_number"
    t.text     "description"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.string   "photograph_file_name"
    t.string   "photograph_content_type"
    t.integer  "photograph_file_size"
    t.datetime "photograph_updated_at"
    t.integer  "presentable_id"
    t.string   "presentable_type"
  end

  add_index "photographs", ["presentable_type", "presentable_id"], name: "index_photographs_on_presentable_type_and_presentable_id", using: :btree

  create_table "private_accommodations", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "name",                  limit: 50
    t.text     "description"
    t.time     "check_in"
    t.time     "check_out"
    t.string   "front_desk",            limit: 20
    t.string   "adult_only",            limit: 20
    t.string   "air_conditioning",      limit: 20
    t.string   "allergy_free",          limit: 20
    t.string   "non_smoking_all",       limit: 20
    t.string   "bar",                   limit: 20
    t.string   "breakfast_buffet",      limit: 20
    t.string   "chapel",                limit: 20
    t.string   "smoking_area",          limit: 20
    t.string   "design_hotel",          limit: 20
    t.string   "elevator",              limit: 20
    t.string   "express_check",         limit: 20
    t.string   "family_rooms",          limit: 20
    t.string   "garden",                limit: 20
    t.string   "gay_friendly",          limit: 20
    t.string   "heating",               limit: 20
    t.string   "luggage_storage",       limit: 20
    t.string   "mini_market",           limit: 20
    t.string   "newspapers",            limit: 20
    t.string   "non_smoking_rooms",     limit: 20
    t.string   "private_beach",         limit: 20
    t.string   "restaurant",            limit: 20
    t.string   "restaurant_carte",      limit: 20
    t.string   "restaurant_buffet",     limit: 20
    t.string   "disabled",              limit: 20
    t.string   "safe",                  limit: 20
    t.string   "kitchen",               limit: 20
    t.string   "shops",                 limit: 20
    t.string   "ski_storage",           limit: 20
    t.string   "snack_bar",             limit: 20
    t.string   "soundproof",            limit: 20
    t.string   "sun_terrace",           limit: 20
    t.string   "terrace",               limit: 20
    t.string   "valet_parking",         limit: 20
    t.string   "aqua",                  limit: 20
    t.string   "bbq",                   limit: 20
    t.string   "beachfront",            limit: 20
    t.string   "billiards",             limit: 20
    t.string   "bowling",               limit: 20
    t.string   "canoeing",              limit: 20
    t.string   "casino",                limit: 20
    t.string   "playground",            limit: 20
    t.string   "cycling",               limit: 20
    t.string   "darts",                 limit: 20
    t.string   "diving",                limit: 20
    t.string   "evening_entertainment", limit: 20
    t.string   "fishing",               limit: 20
    t.string   "fitness",               limit: 20
    t.string   "games",                 limit: 20
    t.string   "golf",                  limit: 20
    t.string   "hiking",                limit: 20
    t.string   "riding",                limit: 20
    t.string   "bath",                  limit: 20
    t.string   "tub",                   limit: 20
    t.string   "pool_all",              limit: 20
    t.string   "pool_season",           limit: 20
    t.string   "pool_swimming",         limit: 20
    t.string   "karaoke",               limit: 20
    t.string   "kids_club",             limit: 20
    t.string   "library",               limit: 20
    t.string   "massage",               limit: 20
    t.string   "minigolf",              limit: 20
    t.string   "out_pool_all",          limit: 20
    t.string   "out_pool_season",       limit: 20
    t.string   "sauna",                 limit: 20
    t.string   "skiing",                limit: 20
    t.string   "ski_school",            limit: 20
    t.string   "snorkeling",            limit: 20
    t.string   "solarium",              limit: 20
    t.string   "spa",                   limit: 20
    t.string   "squash",                limit: 20
    t.string   "table_tennis",          limit: 20
    t.string   "tennis",                limit: 20
    t.string   "steam",                 limit: 20
    t.string   "windsurfing",           limit: 20
    t.string   "airport_shuttle",       limit: 20
    t.string   "atm",                   limit: 20
    t.string   "babysitting",           limit: 20
    t.string   "barber",                limit: 20
    t.string   "bicycle",               limit: 20
    t.string   "room_breakfast",        limit: 20
    t.string   "bridal_suite",          limit: 20
    t.string   "business_centre",       limit: 20
    t.string   "car_rental",            limit: 20
    t.string   "concierge",             limit: 20
    t.string   "exchange",              limit: 20
    t.string   "maid",                  limit: 20
    t.string   "dry_cleaning",          limit: 20
    t.string   "entertainment_staff",   limit: 20
    t.string   "fax",                   limit: 20
    t.string   "groceries",             limit: 20
    t.string   "ironing",               limit: 20
    t.string   "laundry",               limit: 20
    t.string   "lockers",               limit: 20
    t.string   "meeting",               limit: 20
    t.string   "nightclub",             limit: 20
    t.string   "lunches",               limit: 20
    t.string   "private_check",         limit: 20
    t.string   "room_service",          limit: 20
    t.string   "lounge",                limit: 20
    t.string   "shoe_shine",            limit: 20
    t.string   "shuttle",               limit: 20
    t.string   "ski_hire",              limit: 20
    t.string   "ski_pass",              limit: 20
    t.string   "ski_to_door",           limit: 20
    t.string   "souvenirs",             limit: 20
    t.string   "special_diet",          limit: 20
    t.string   "tickets",               limit: 20
    t.string   "tour",                  limit: 20
    t.string   "trouser_press",         limit: 20
    t.string   "vending_drinks",        limit: 20
    t.string   "vending_snacks",        limit: 20
    t.string   "vip_room",              limit: 20
    t.string   "water_sports",          limit: 20
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "rent_pricing_schemes", force: :cascade do |t|
    t.integer "vehicle_id"
    t.string  "name",         limit: 50
    t.integer "base_rate",    limit: 8
    t.integer "weekly_rate",  limit: 8
    t.integer "monthly_rate", limit: 8
    t.integer "yearly_rate",  limit: 8
  end

  create_table "rents", force: :cascade do |t|
    t.string   "name",        limit: 50
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "reservation_packages", force: :cascade do |t|
    t.integer  "user_id"
    t.date     "date_from"
    t.date     "date_to"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "reservations", force: :cascade do |t|
    t.integer  "reservable_id"
    t.string   "reservable_type"
    t.integer  "reservation_package_id"
    t.datetime "date_from"
    t.datetime "date_to"
    t.integer  "number_of_people"
    t.decimal  "price",                  precision: 7, scale: 2, default: 0.0, null: false
    t.integer  "status",                                         default: 0,   null: false
    t.text     "notes"
  end

  create_table "room_pricing_schemes", force: :cascade do |t|
    t.string   "name",                          limit: 50,                         null: false
    t.decimal  "base_rate",                                precision: 7, scale: 2, null: false
    t.decimal  "weekly_rate",                              precision: 7, scale: 2
    t.decimal  "extra_guests_amount",                      precision: 7, scale: 2
    t.integer  "min_length"
    t.integer  "long_stay_discount_threshold"
    t.decimal  "long_stay_discount_percentage",            precision: 2, scale: 2
    t.datetime "created_at",                                                       null: false
    t.datetime "updated_at",                                                       null: false
    t.integer  "room_id"
  end

  add_index "room_pricing_schemes", ["room_id"], name: "index_room_pricing_schemes_on_room_id", using: :btree

  create_table "rooms", force: :cascade do |t|
    t.integer  "lodging_provider_id",                          null: false
    t.string   "lodging_provider_type",                        null: false
    t.string   "name",                  limit: 50,             null: false
    t.text     "description",                                  null: false
    t.string   "air_conditioning",      limit: 20
    t.string   "cleaning_products",     limit: 20
    t.string   "walk_in_closet",        limit: 20
    t.string   "fan",                   limit: 20
    t.string   "hot_tub",               limit: 20
    t.string   "ironing",               limit: 20
    t.string   "priv_ent",              limit: 20
    t.string   "sitting",               limit: 20
    t.string   "tile_floor",            limit: 20
    t.string   "parquet_floor",         limit: 20
    t.string   "hypoallergenic",        limit: 20
    t.string   "dryer",                 limit: 20
    t.string   "el_blankets",           limit: 20
    t.string   "fireplace",             limit: 20
    t.string   "interconnecting",       limit: 20
    t.string   "mosquito",              limit: 20
    t.string   "pool",                  limit: 20
    t.string   "sofa",                  limit: 20
    t.string   "wardrobe",              limit: 20
    t.string   "carpeted",              limit: 20
    t.string   "desk",                  limit: 20
    t.string   "long_beds",             limit: 20
    t.string   "heating",               limit: 20
    t.string   "iron",                  limit: 20
    t.string   "suit",                  limit: 20
    t.string   "safe",                  limit: 20
    t.string   "soundproof",            limit: 20
    t.string   "wash",                  limit: 20
    t.string   "additional",            limit: 20
    t.string   "bathtub_shower",        limit: 20
    t.string   "toiletries",            limit: 20
    t.string   "shared_bathroom",       limit: 20
    t.string   "slippers",              limit: 20
    t.string   "bathroom",              limit: 20
    t.string   "guest_bathroom",        limit: 20
    t.string   "bathrobe",              limit: 20
    t.string   "hairdryer",             limit: 20
    t.string   "shared_toilet",         limit: 20
    t.string   "spa_tub",               limit: 20
    t.string   "bathtub",               limit: 20
    t.string   "bidet",                 limit: 20
    t.string   "sauna",                 limit: 20
    t.string   "shower",                limit: 20
    t.string   "toilet",                limit: 20
    t.string   "blu_ray",               limit: 20
    t.string   "computer",              limit: 20
    t.string   "flat_tv",               limit: 20
    t.string   "console",               limit: 20
    t.string   "console_ps2",           limit: 20
    t.string   "console_ps3",           limit: 20
    t.string   "console_ps4",           limit: 20
    t.string   "console_wii",           limit: 20
    t.string   "console_xbox",          limit: 20
    t.string   "laptop",                limit: 20
    t.string   "radio",                 limit: 20
    t.string   "telephone",             limit: 20
    t.string   "ipad",                  limit: 20
    t.string   "cd_play",               limit: 20
    t.string   "dvd_play",              limit: 20
    t.string   "laptop_safe",           limit: 20
    t.string   "satellite",             limit: 20
    t.string   "video",                 limit: 20
    t.string   "ipod_dock",             limit: 20
    t.string   "cable",                 limit: 20
    t.string   "fax",                   limit: 20
    t.string   "pay_channels",          limit: 20
    t.string   "tv",                    limit: 20
    t.string   "video_games",           limit: 20
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
    t.integer  "max_adults",            limit: 8,  default: 0
    t.integer  "max_children",          limit: 8,  default: 0
    t.integer  "size",                  limit: 8
    t.integer  "single_beds",           limit: 8,  default: 0
    t.integer  "double_beds",           limit: 8,  default: 0
    t.integer  "queen_beds",            limit: 8,  default: 0
    t.integer  "king_beds",             limit: 8,  default: 0
    t.integer  "bunk_beds",             limit: 8,  default: 0
    t.integer  "sofa_beds",             limit: 8,  default: 0
    t.integer  "futons",                limit: 8,  default: 0
    t.string   "barbecue",              limit: 20
    t.string   "dining_table",          limit: 20
    t.string   "high_chair",            limit: 20
    t.string   "kitchenware",           limit: 20
    t.string   "outdoor_dining",        limit: 20
    t.string   "refrigerator",          limit: 20
    t.string   "toaster",               limit: 20
    t.string   "coffee",                limit: 20
    t.string   "dishwasher",            limit: 20
    t.string   "kitchen",               limit: 20
    t.string   "microwave",             limit: 20
    t.string   "outdoor_furniture",     limit: 20
    t.string   "stovetop",              limit: 20
    t.string   "dining",                limit: 20
    t.string   "kettle",                limit: 20
    t.string   "kitchenette",           limit: 20
    t.string   "minibar",               limit: 20
    t.string   "oven",                  limit: 20
    t.string   "tea_coffee",            limit: 20
    t.string   "alarm_clock",           limit: 20
    t.string   "towels",                limit: 20
    t.string   "executive",             limit: 20
    t.string   "towels_extra",          limit: 20
    t.string   "linens",                limit: 20
    t.string   "wake_up",               limit: 20
    t.string   "city_view",             limit: 20
    t.string   "landmark_view",         limit: 20
    t.string   "pool_view",             limit: 20
    t.string   "terrace",               limit: 20
    t.string   "garden_view",           limit: 20
    t.string   "mountain_view",         limit: 20
    t.string   "river_view",            limit: 20
    t.string   "view",                  limit: 20
    t.string   "lake_view",             limit: 20
    t.string   "patio",                 limit: 20
    t.string   "sea_view",              limit: 20
    t.string   "balcony",               limit: 20
    t.string   "smoking_allowed",       limit: 20
  end

  create_table "trip_destinations", force: :cascade do |t|
    t.integer  "trip_id"
    t.string   "city_id"
    t.date     "start_date"
    t.date     "end_date"
    t.integer  "no_of_guest"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "trip_destinations", ["trip_id"], name: "index_trip_destinations_on_trip_id", using: :btree

  create_table "trip_items", force: :cascade do |t|
    t.integer  "trip_destination_id"
    t.string   "item_id"
    t.string   "item_type"
    t.decimal  "price"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.date     "start_date"
    t.date     "end_date"
  end

  add_index "trip_items", ["trip_destination_id"], name: "index_trip_items_on_trip_destination_id", using: :btree

  create_table "trips", force: :cascade do |t|
    t.integer  "user_id"
    t.boolean  "active",      default: false
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.date     "start_date"
    t.date     "end_date"
    t.integer  "no_of_guest"
    t.datetime "expires"
  end

  add_index "trips", ["user_id"], name: "index_trips_on_user_id", using: :btree

  create_table "user_subscriptions", force: :cascade do |t|
    t.string   "email"
    t.boolean  "is_business"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "users", force: :cascade do |t|
    t.datetime "created_at",                                         null: false
    t.datetime "updated_at",                                         null: false
    t.string   "email",                              default: "",    null: false
    t.string   "encrypted_password",                 default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                      default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "first_name",             limit: 50
    t.string   "last_name",              limit: 50
    t.integer  "oib",                    limit: 8
    t.string   "telephone_number",       limit: 50
    t.string   "gender",                 limit: 50
    t.date     "birth_date"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.string   "company_name",           limit: 100
    t.string   "vat_id",                 limit: 50
    t.boolean  "is_business",                        default: false
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "vehicles", force: :cascade do |t|
    t.integer  "rent_id"
    t.string   "type"
    t.string   "name",        limit: 50
    t.text     "description"
    t.integer  "amount",      limit: 8
    t.integer  "capacity",    limit: 8
    t.integer  "engine_cc",   limit: 8
    t.integer  "engine_hp",   limit: 8
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "insurance"
  end

end
